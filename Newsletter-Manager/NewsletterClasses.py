import sqlite3

class NewsletterManager:
    newsletters = []
    currentNewsletterID = None
    currentNewsletter = None
    sqlConnection = None
    allContent = None

    def fetchAllNewsletters():
        c = NewsletterManager.getSQLiteConnection()
        NewsletterManager.newsletters = []
        c.execute("SELECT * FROM newsletters ORDER BY title_id")
        for row in c.fetchall():
            Newsletter(row[0],row[1])
                                                 
        NewsletterManager.sqlConnection.commit()
        NewsletterManager.sqlConnection.close()
    

    def resetAllData():
        c = NewsletterManager.getSQLiteConnection()
        c.execute('DROP TABLE IF EXISTS newsletters')
        c.execute('DROP TABLE IF EXISTS primary_blocks')
        c.execute('DROP TABLE IF EXISTS secondary_blocks')
        c.execute('CREATE TABLE newsletters (title_id TEXT PRIMARY KEY, full_title TEXT)')
        c.execute('''CREATE TABLE primary_blocks
    (unique_code PRIMARY KEY, newsletter_id, full_title, image_url, body, link_url, style, reusable)''')
        c.execute('''CREATE TABLE secondary_blocks
    (unique_code PRIMARY KEY, newsletter_id, full_title, image_url, body, link_url, style, reusable)''')
        NewsletterManager.sqlConnection.commit()
        NewsletterManager.sqlConnection.close()
        newsletter = Newsletter('sample1',"Sample Newsletter")
        newsletter.addContentBlock("primary", "Sample Primary Block", "http://alexb.ddns.me/ics-ux-page/assets/ux-services/objective.png", "<p>Sample Body Paragraph</p>")
        NewsletterManager.saveCurrentNewsletter(newsletter)

    def saveCurrentNewsletter(newsletter = None):
        if newsletter == None:
            newsletter = NewsletterManager.currentNewsletter
        c = NewsletterManager.getSQLiteConnection()
        c.execute("""INSERT OR REPLACE INTO newsletters
    (title_id, full_title) VALUES ('""" +
                  newsletter.titleId + "','" +
                  newsletter.fullTitle + "')")
        
        c.execute("DELETE FROM primary_blocks WHERE newsletter_id='" + newsletter.titleId + "'")
        c.execute("DELETE FROM secondary_blocks WHERE newsletter_id='" + newsletter.titleId + "'")
        
        
        for primaryBlock in newsletter.primaryContent:
                
            c.execute("INSERT INTO primary_blocks" +
                      "(unique_code, newsletter_id, full_title, image_url, body, link_url, style, reusable) VALUES (" +
                      "'n" + repr(primaryBlock.uniqueIDList[0]) +
                      "p" + repr(primaryBlock.uniqueIDList[1]) + "','" +
                      primaryBlock.parentNewsletter.titleId + "','" +
                      primaryBlock.title + "','" +
                      primaryBlock.imageUrl + "','" +
                      primaryBlock.body + "','" +
                      primaryBlock.url + "'," +
                      repr(primaryBlock.style) + ",'" +
                      repr(primaryBlock.reusable) + "')")

        for secondaryBlock in newsletter.secondaryContent:
                
            c.execute("INSERT INTO secondary_blocks" +
                      "(unique_code, newsletter_id, full_title, image_url, body, link_url, style, reusable) VALUES (" +
                      "'n" + repr(secondaryBlock.uniqueIDList[0]) +
                      "s" + repr(secondaryBlock.uniqueIDList[1]) + "','" +
                      primaryBlock.parentNewsletter.titleId + "','" +
                      secondaryBlock.title + "','" +
                      secondaryBlock.imageUrl + "','" +
                      secondaryBlock.body + "','" +
                      secondaryBlock.url + "'," +
                      repr(secondaryBlock.style) + ",'" +
                      repr(secondaryBlock.reusable) + "')")
            
        NewsletterManager.sqlConnection.commit()
        NewsletterManager.sqlConnection.close()
        
    def fetchContent(content_type, newsletter):
        c = NewsletterManager.getSQLiteConnection()
        if content_type == "primary":
            newsletter.primaryContent = []
        if content_type == "secondary":
            newsletter.secondaryContent = []
        for row in c.execute("SELECT * FROM " + content_type + "_blocks WHERE newsletter_id = '" + newsletter.titleId + "'"):
            if newsletter.titleId == row[1]:
                newsletter.addContentBlock(content_type, row[2], row[3], row[4], row[5], row[6], row[0], row[7])
        NewsletterManager.sqlConnection.commit()
        NewsletterManager.sqlConnection.close()
        return

    def fetchReuseableBlocks(content_type, newsletter):
        c = NewsletterManager.getSQLiteConnection()
        if content_type == "primary":
            newsletter.primaryContent = []
        if content_type == "secondary":
            newsletter.secondaryContent = []
        for row in c.execute("SELECT * FROM " + content_type + "_blocks WHERE reusable = 'True'"):
            if newsletter.titleId == row[1]:
                newsletter.addContentBlock(content_type, row[2], row[3], row[4], row[5], row[6], row[0], row[7])
        NewsletterManager.sqlConnection.commit()
        NewsletterManager.sqlConnection.close()

    def populateCurrentNewsletter():
        NewsletterManager.fetchContent("primary", NewsletterManager.currentNewsletter)
        NewsletterManager.fetchContent("secondary", NewsletterManager.currentNewsletter)
        return
    
    def test():
        NewsletterManager.resetAllData()
        print("Test: Reset Data")
        news = Newsletter('jan12', "January dsdd")
        print("Test: news = Newsletter()")
        news.addContentBlock("primary", "Hello World", "http://alexb.ddns.me/ics-ux-page/assets/ux-services/objective.png")
        print("Test: Added primary block titled Hello World")
        NewsletterManager.currentNewsletter = NewsletterManager.newsletters[0]
        print("Test: Set news as current newsletter")
        NewsletterManager.saveCurrentNewsletter()
        print("Test: Attempted to save")
        #NewsletterManager.resetAllData()
        print("Test: Reset of data")
        NewsletterManager.fetchAllNewsletters()
        print("Test: List of all newsletters")

    def getSQLiteConnection():
        from os import path
        NewsletterManager.sqlConnection = sqlite3.connect(path.dirname(path.realpath(__file__)) + '/newsletters.db')
        cursor = NewsletterManager.sqlConnection.cursor()
        return cursor

    
     
class Newsletter:
    def __init__(self, title_id, full_title, primaryContent = [], secondaryContent = []):
        self.titleId = title_id
        self.fullTitle = full_title
        self.primaryContent = primaryContent
        self.secondaryContent = secondaryContent
        NewsletterManager.newsletters.append(self)

    def addContentBlock(self, contentType, title, imageUrl = '', body = '', url = '', style = 0, fromDB = False, reusable = False):
        if contentType == "primary":
            newBlock = PrimaryContentBlock(self, title, imageUrl, body, url, style, reusable)
            self.primaryContent.append(newBlock)
        elif contentType == "secondary":
            newBlock = SecondaryContentBlock(self, title, imageUrl, body, url, style, reusable)
            self.secondaryContent.append(newBlock)
        if fromDB == False:
            newBlock.setUniqueID()
        elif fromDB[0:3] == "new":
            newBlock.setUniqueID()
        else:
            loc = None
            if contentType == "primary":
                loc = fromDB.rfind('p')
            elif contentType == "secondary":
                loc = fromDB.rfind('s')
            
            uniqueId = [fromDB[1:loc],fromDB[loc + 1:]]
            newBlock.setUniqueID(uniqueId)

    def fetchContent():
        pass

class ContentBlock:
    uniqueID = None
    uniqueIDList = ['',''] ## Newsletter ID, Index
    def __init__(self, newsletter, title, imageUrl = '', body = '', url = '', style = 0, reusable = False):
        self.title = title
        self.imageUrl = imageUrl
        self.body = body
        self.url = url
        self.parentNewsletter = newsletter
        self.style = style
        self.reusable = reusable
        if self.reusable in ["True", "true", "1", True, 1]:
            self.reusable = True
        else:
            self.reusable = False

        
class PrimaryContentBlock(ContentBlock):
    def setUniqueID(self, uniqueID = None):
        if uniqueID == None:
            self.uniqueIDList = [NewsletterManager.newsletters.index(self.parentNewsletter),self.parentNewsletter.primaryContent.index(self)]
        else:
            self.uniqueIDList = uniqueID
        self.uniqueID = "n" + str(self.uniqueIDList[0]) + "p" + str(self.uniqueIDList[1])
        
class SecondaryContentBlock(ContentBlock):
    def setUniqueID(self, uniqueID = None):
        if uniqueID == None:
            self.uniqueIDList = [NewsletterManager.newsletters.index(self.parentNewsletter),self.parentNewsletter.secondaryContent.index(self)]
        else:
            self.uniqueIDList = uniqueID
        self.uniqueID = "n" + str(self.uniqueIDList[0]) + "s" + str(self.uniqueIDList[1])
        
        
