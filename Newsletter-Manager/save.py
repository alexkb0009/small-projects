import os, cgi
from NewsletterClasses import NewsletterManager, Newsletter, PrimaryContentBlock, SecondaryContentBlock
from render import Renderer

print('Status: 200 OK')
print('Content-type: text/html')
print('')

query = os.environ.get('QUERY_STRING')
arguments = cgi.parse_qs(query)
form = cgi.FieldStorage()

print("""
<html>
<head>
<title>Newsletter Manager</title>
<style type='text/css' media='all'>
  @import url('newsletter-manager.css');
</style>
</head>
<body>
<div id="header">
  <a href="index.py">
    <h3 class="header-item">
      <span>ICS Inc. Newsletter Manager</span>
    </h3>
  </a>
</div>
""")

orderedActions = []
length = 0
try:
    if isinstance(form["unique"], list):
        length = len(form["unique"])
    else:
        length = 1
except:
    length = 0
    
newsletterId = None
counter = 0

NewsletterManager.fetchAllNewsletters()
for nl in NewsletterManager.newsletters:
    if nl.titleId == form["newsletter"].value:
        NewsletterManager.currentNewsletter = nl
        newsletterId = nl.titleId
        break
else:
    try:
        if form['mode'].value == 'new':
            newsletterId = "nl-" + str(len(NewsletterManager.newsletters))
            NewsletterManager.currentNewsletter = Newsletter(newsletterId, form["newsletterTitle"].value)
    except:
        print("Error: Newsletter not found or created.")

NewsletterManager.currentNewsletter.fullTitle = form["newsletterTitle"].value

while counter < length:
    currBlockID = None
    if length == 1:
        currBlockID = form["unique"].value
    else:
        currBlockID = form["unique"][counter].value
    currAction = form["action[" + currBlockID + "]"].value
    currContentType = form["content-type[" + currBlockID + "]"].value
    currStyle = form["style[" + currBlockID + "]"].value
    currTitle = None
    currBody = None
    currImageUrl =  None
    currUrl = None
    currReusable = False
    if form["reusable[" + currBlockID + "]"].value in ["true", "True", True]:
        currReusable = True
    
    try:
        currTitle = form["title[" + currBlockID + "]"].value
    except KeyError:
        currTitle = ''
    try:
        currBody = form["body[" + currBlockID + "]"].value.replace('\n','').replace('\r','').rstrip()
    except KeyError:
        currBody = ''
    try:
        currImageUrl = form["imageUrl[" + currBlockID + "]"].value
    except KeyError:
        currImageUrl = ''
    try:
        currUrl = form["url[" + currBlockID + "]"].value
    except KeyError:
        currUrl = ''
        
    orderedActions.append(currAction)
    if currAction == 'update':
        NewsletterManager.currentNewsletter.addContentBlock(currContentType, currTitle, currImageUrl, currBody, currUrl, currStyle, False, currReusable)

#    print(currBlockID + " (" + str(currTitle) + "):")
#    print("action:     " + str(currAction))
#    print("style:      " + str(currStyle))
#    print("image url:  " + str(currImageUrl))
#    print("link url:   " + str(currUrl))
#    print("body:")
#    print(currBody)
#    print()

    counter = counter + 1

NewsletterManager.saveCurrentNewsletter()

print('<div id="page-preface">')
print('Newsletter ' + NewsletterManager.currentNewsletter.fullTitle + ' Saved!')
print('<div class="right" style="float: right">')
print('<img src="./graphics/gnome_icons/22/document-page-setup.png" style="margin: -1px 8px;float: left;">')
print('<a href="index.py?name=' + newsletterId + '&mode=edit">Go back to edit.</a></div>')
print('</div>')

if form["postResponse"].value == "home":
    print('<div id="page-body" class="redirect">')
    print("""
    <script type="text/javascript">
    window.location.assign("index.py");
    </script>
    """)
    print('</div>')

elif form["postResponse"].value == "back":
    print('<div id="page-body" class="redirect">')
    print('<script type="text/javascript">')
    print('window.location.assign("index.py?name=' + newsletterId + '&mode=edit");')
    print('</script>')
    print('</div>')

elif form["postResponse"].value == "view":
    print('<div id="page-body" class="view redirect">')
    print('<h4 style="text-align: center; border-bottom: 1px dotted #bbb; padding-bottom: 40px; font-family: Roboto;font-weight: 300;">Redirecting to Email Preview/HTML page in 5 seconds.</h4><br>')
    print('<div id="newsletter-html">')
    print(Renderer.writeHTML())
    print('</div>')
    print('<script type="text/javascript">')
    print('window.setTimeout(function(){')
    print('  window.location.assign("render.py?newsletter=' + newsletterId + '");')
    print('},5000);')
    print('</script>')
    print('</div>')





