var ux;

$(document).ready(function(){

ux = {
  markForDeletion: function(parentElem, uniqueId, minTab){
    parentElem = $(this).parent().parent();
    uniqueId = parentElem.attr("block");
    if (!parentElem.hasClass("marked-deletion")){
      parentElem.addClass("marked-deletion");
      this.innerHTML = "marked for deletion : undo";
      parentElem.children('input#action-input-' + uniqueId).attr("value", "delete");
    } else {
      parentElem.removeClass("marked-deletion");
      parentElem.children('input#action-input-' + uniqueId).attr("value", "update");
      this.innerHTML = "delete";
      minTab = parentElem.children('.tab-container').children('.tab.minimize');
      if (minTab.html() == "+") minTab.click();
    }
    return false;
  },
  minimize: function(){
    parentElem = $(this).parent().parent();
    uniqueId = parentElem.attr("block");
    if (!parentElem.hasClass("minimized")){
      parentElem.addClass("minimized");
      this.innerHTML = "+";
    } else {
      parentElem.removeClass("minimized");
      this.innerHTML = "-";
    }
    return false;
  },
  reusable: function(){
    parentElem = $(this).parent().parent();
    uniqueId = parentElem.attr("block");
    if (!parentElem.hasClass("reusable")){
      parentElem.addClass("reusable");
      parentElem.children('input#reusable-' + uniqueId).val("true");
    } else {
      parentElem.removeClass("reusable");
      parentElem.children('input#reusable-' + uniqueId).val("false");
    }
    return false;
  },
  newCounter: 0,
  insertPrimaryBlock: function(){
    ux.insertBlock("primary");
    $("div.primary-content .delete.tab").off().click(ux.markForDeletion);
    $("div.primary-content .minimize.tab").off().click(ux.minimize);
	$("div.primary-content .reusable.tab").off().click(ux.reusable);
    return false;
  },
  insertSecondaryBlock: function(){
    ux.insertBlock("secondary");
    $("div.secondary-content .delete.tab").off().click(ux.markForDeletion);
    $("div.secondary-content .minimize.tab").off().click(ux.minimize);
	$("div.secondary-content .reusable.tab").off().click(ux.reusable);
    return false;
  },
  insertBlock: function(type, uniqueId, title, imageUrl, body, url, style, reusable, minimized, blockhtml){
    if (uniqueId == null){
      uniqueId = "new" + ++ux.newCounter;
      title = '';
      imageUrl = '';
      body = '';
      url = '';
      style = 0;
      reusable = false;
	  minimized = false;
    }
	if (minimized == null){
	  minimized = true;
	}

    blockhtml = '<div class="' + type + '-content input centered-width ' + (minimized ? "minimized" : "") + ' ' + (reusable == "True"? "reusable" : "") + '" block="' + uniqueId + '">';
    blockhtml += '<input type="hidden" name="unique" value="' + uniqueId + '">';
    blockhtml += '<input type="hidden" name="content-type[' + uniqueId + ']" value="' + type + '">';
    blockhtml += '<input type="hidden" id="action-input-' + uniqueId + '" name="action[' + uniqueId + ']" value="update">';
    blockhtml += '<input type="hidden" id="reusable-' + uniqueId + '" name="reusable[' + uniqueId + ']" value="' + reusable + '">';
    blockhtml += '<div class="tab-container">';
    blockhtml += '<div class="minimize tab">' + (minimized ? "+" : "-") + '</div>';
    blockhtml += '<div class="delete tab">delete</div>';
    //blockhtml += '<div class="tab">' + uniqueId + '</div>';
    //blockhtml += '<div class="tab">' + type + '</div>';
    blockhtml += '<div class="reusable tab">reusable</div>';
    blockhtml += '</div><table>';
        
    blockhtml += '<tr><td style="width: 33%;" class="title-heading"><label>Heading: </label></td>';
    blockhtml += '<td style="width: 67%;" class="title-input"><input type="text" name="title[' + uniqueId + ']" value="' + title + '"></td></tr>';
        
    blockhtml += '<tr><td style="width: 33%; vertical-align: middle;" class="reg-item header-image"><label>Header Image URL: </label>';
    if (imageUrl != '') blockhtml += '<br><img src="' + imageUrl + '">';
    blockhtml += '</td><td style="width: 67%;" class="text-input reg-item"><input type="text" name="imageUrl[' + uniqueId + ']" value="' + imageUrl + '">';
    blockhtml += '</td></tr>';
    if (type == "secondary"){
      blockhtml += '<tr><td style="width: 33%;" class="link-url reg-item"><label>Link URL: </label></td>';
      blockhtml += '<td style="width: 67%;" class="url-input reg-item"><input type="text" name="url[' + uniqueId + ']" value="' + url + '"></td></tr>';
    } else {
      blockhtml += '<input type="hidden" name="url[' + uniqueId + ']" value="">'
    }
    blockhtml += '<tr><td style="width: 33%; vertical-align: middle;" class="reg-item block-style"><label>Style or Format: </label>';
    blockhtml += '</td><td style="width: 67%;" class="reg-item"><select name="style[' + uniqueId + ']">';
    blockhtml += '<option value="0" ' + (style == 0 ? ' selected="true"' : '') + '>(1) Default: Header with image to left of title, followed by text.</option>';
    blockhtml += '<option value="1"' + (style == 1 ? ' selected="true"' : '') + '> (2) Header with image to left of title, followed by text.</option>';
    blockhtml += '<option value="2"' + (style == 2 ? ' selected="true"' : '') + '> (3) Full Width Image</option>';
    blockhtml += '</select></td></tr>';
    

    blockhtml += '<tr><td colspan=2 class="text-input body"><label style="display: block;">Body:</label><textarea name="body[' + uniqueId + ']" rows="10" id="body-action-input-' + uniqueId + '">';
    blockhtml += body;
    blockhtml += '</textarea></td></tr></table></div>';
    
    $("div#" + type + "-items").append(blockhtml);
    CKEDITOR.replace('body-action-input-' + uniqueId);
  },
  preSubmit: function(form){
  /*
    $("#page-body.edit input#postResponse").val(function(i,v){
      return $.trim(v) === '' ? 'none-specified' : v;
    });
    */
    $("#save-button-actual").click();
  },
  resetEditor: function(e, ui, uniqueId){
    uniqueId = $(ui.item[0]).attr("block");
    if(CKEDITOR.instances['body-action-input-' + uniqueId]){
	  CKEDITOR.instances['body-action-input-' + uniqueId].destroy();
	}
  },
  setEditor: function(e, ui, uniqueId){
    uniqueId = $(ui.item[0]).attr("block");
    if(!CKEDITOR.instances['body-action-input-' + uniqueId]){
	  CKEDITOR.replace('body-action-input-' + uniqueId);
	}
  },
  bind: function(){
    $(".delete.tab").click(ux.markForDeletion);
    $(".minimize.tab").click(ux.minimize);
    $(".reusable.tab").click(ux.reusable);
    $("#save-button, #top-save-button").click(ux.preSubmit);
    $("form#edit-newsletter-form .primary.add-new").click(ux.insertPrimaryBlock);
    $("form#edit-newsletter-form .secondary.add-new").click(ux.insertSecondaryBlock);
    $("#primary-items").sortable({
	  stop: ux.setEditor,
	  start: ux.resetEditor
	}).disableSelection();
    $("#secondary-items").sortable({ 
	  stop: ux.setEditor,
	  start: ux.resetEditor
	}).disableSelection();
  },
  init: function(){
    if (typeof nlData === 'undefined') return;
    for (var i = 0; i < nlData.primary.length; i++){
      var pi = nlData.primary[i];
      ux.insertBlock("primary", pi.uniqueID, pi.title, pi.imageUrl, pi.body, pi.url, pi.style, pi.reusable);
    }
    for (var i = 0; i < nlData.secondary.length; i++){
      var si = nlData.secondary[i];
      ux.insertBlock("secondary", si.uniqueID, si.title, si.imageUrl, si.body, si.url, si.style, si.reusable);
    }
  }
}

ux.init();
ux.bind();

});

