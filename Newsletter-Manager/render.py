import os, cgi
from NewsletterClasses import NewsletterManager, Newsletter, PrimaryContentBlock, SecondaryContentBlock
import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

width = 800
widthLeft = 520
widthRight = 280
primaryContentImageSize = 62
class Renderer:

    def init():
        query = os.environ.get('QUERY_STRING')
        Renderer.arguments = cgi.parse_qs(query)
        NewsletterManager.fetchAllNewsletters()
        try:
            for nl in NewsletterManager.newsletters:
                if nl.titleId == Renderer.arguments['newsletter'][0]: # Found Newsletter
                    NewsletterManager.currentNewsletter = nl
                    NewsletterManager.populateCurrentNewsletter()
                    return NewsletterManager.currentNewsletter
            else:
                return False
        except:
            print("Error: No newsletter specified.")
            return False
        

    def output():
        print('Status: 200 OK')
        print('Content-type: text/html')
        print('')
        newsletter = Renderer.init()
        if not newsletter:
            return False
        newsletterHTML = Renderer.writeHTML(newsletter)
        print('''
        <html>
        <head>
        <title>Newsletter Manager</title>
        <style type='text/css' media='all'>
          @import url('newsletter-manager.css');
        </style>
        </head>
        <body>
        <div id="header" style="position: absolute;">
          <a href="index.py">
            <h3 class="header-item">
              <span>ICS Inc. Newsletter Manager</span>
            </h3>
          </a>
        ''')

        print('<a href="index.py?name=' + NewsletterManager.currentNewsletter.titleId + '&mode=edit"><h4 class="header-item newsletter-name" style="border-right: 1px dotted #ccc;">')
        print('<span><img src="./graphics/gnome_icons/24/document-page-setup.png" style="margin-bottom: -4px; margin-left: 4px;">' + newsletter.fullTitle + '</span></h4></a>')

        print("""
          <div class="header-item">
            <form name="newsletter-select" action="render.py" method="get">
              <label>Go to:</label>
              <select name="newsletter">
        """)

        for nl in NewsletterManager.newsletters:
            if NewsletterManager.currentNewsletter.titleId == nl.titleId:
                print('<option value="' + nl.titleId + '" selected="true">' + nl.fullTitle + '</option>')
            else: print('<option value="' + nl.titleId + '">' + nl.fullTitle + '</option>')

        print("""
              </select>
              <input type="hidden" name="mode" value="view">
              <button type="submit">View</button>
            </form>
          </div>
        </div>
        """)
        
        print('<table width="100%" height="100%" cellpadding="0" cellspacing="0" style="width:100%;height:100%;">')
        print('<tr><td colspan="2" style="height: 51px;">&nbsp;</td></tr>')
        print('<tr><td id="render-info">')
        print('<label>Source: </label>')
        print('<textarea id="html-output-area" style="width: 100%; height: 400px;">')
        print(newsletterHTML)
        print('</textarea>')
        Renderer.emailUITest(newsletterHTML)
        print('</td><td id="render-viewport" style="min-width: ' + str(width) +'px">')
        print('<div id="viewport-inner">' + newsletterHTML + '</div>')
        print('</td></tr></table>')
        print('</body></html>')


    def writeHTML(nl = None):
        if nl == None:
            nl = NewsletterManager.currentNewsletter
        newsletterWidth = str(width)
        html = '<table width="'+ newsletterWidth + '" border="0" cellpadding="0" cellspacing="0" style="font-family: Trebuchet MS, HelveticaNeue, Helvetica, sans-serif; width: '+ newsletterWidth + 'px; border-spacing: 0; border: none; background-color: #F7F7F7;">'

        html = html + '<tr><td>'
        html = html + '<table width="'+ newsletterWidth + '" border="0" cellpadding="7" cellspacing="0" style="width: '+ newsletterWidth + 'px; border-spacing: 0; border: none;">'
        html = html + '<tr><td width="250" style="width: 250px;">'
        html = html + '<a href="http://www.ics.com">'
        html = html + '<img src="http://www.ics.com/sites/default/files/logotext_0.png" alt="ICS - Integrated Computer Solutions" border="0" style="border: none;">'
        html = html + '</a>'
        html = html + '</td><td style="background: url(http://www.ics.com/sites/all/themes/ics/images/newsletter-header-bg.png) no-repeat 100% 50%;">'
        html = html + '&nbsp;&nbsp;</td></tr></table></td></tr>'
        
        html = html + '<tr><td>'
        html = html + '<table width="'+ newsletterWidth + '" border="0" cellpadding="12" cellspacing="0" style="width: '+ newsletterWidth + 'px; border-spacing: 0; border: none; border-bottom: 1px dotted #000000; border-top: 1px dotted #000000;">'
        html = html + '<tr><td style="text-align: left; margin: 0; font-size: .86em; font-weight: 500; color: #727272;">'
        html = html + 'NEWSLETTER / <a href="http://www.ics.com/company/newsletters" style="text-decoration: none;">View All</a>'
        html = html + '</td><td style="font-size: 1.2em; text-align: right; font-weight: 400; margin: 0; color: #000;">'
        html = html + nl.fullTitle
        html = html + '</td></tr>'
        html = html + '</table>'
        html = html + '</td></tr>'
        
        html = html + '<tr><td>'
        html = html + '<table width="'+ newsletterWidth + '" border="0" cellpadding="0" cellspacing="0" style="width: '+ newsletterWidth + 'px; border-spacing: 0; border: none;">'
        html = html + '<tr><td width="' + str(widthLeft) + '" style="width: ' + str(widthLeft) + 'px;">'
        html = html + Renderer.primaryContentHTML()
        html = html + '</td><td width="' + str(widthRight) + '" valign="top" style="width: ' + str(widthRight) + 'px; vertical-align: top;">'
        html = html + Renderer.secondaryContentHTML()
        html = html + '</td></tr>'
        html = html + '</table>'
        html = html + '</td></tr>'
        
        html = html + '</table>'
        return html

    def primaryContentHTML():
        htmlPart = '<table cellpadding="0" cellspacing="0" border="0" style="width: '+ str(widthLeft) + 'px; border-spacing: 0;">'
        for pb in NewsletterManager.currentNewsletter.primaryContent:
            htmlPart = htmlPart + '<tr><th style="text-align: left;">'
            if pb.imageUrl not in ['', None, False]:
                htmlPart = htmlPart + '<img src="' + pb.imageUrl + '" align="left" width="' + str(primaryContentImageSize) + '" height="' + str(primaryContentImageSize) + '" style="width: ' + str(primaryContentImageSize) + 'px; height: ' + str(primaryContentImageSize) + 'px; float: left; padding: 5px 14px 5px 5px;">'
            else:
                htmlPart = htmlPart + '&nbsp;&nbsp;'
            htmlPart = htmlPart + '<h3 style="font-weight: 500; padding: 24px 0 5px 10px; margin: 0; font-size: 13.5pt;">'
            htmlPart = htmlPart + pb.title
            htmlPart = htmlPart + '</h3></th></tr>'
            
            htmlPart = htmlPart + '<tr><td style="padding: 10px 18px; font-size: 10pt; border-top: 1px dotted #cccccc;">'
            htmlPart = htmlPart + pb.body
            htmlPart = htmlPart + '</td></tr>'

        htmlPart = htmlPart + '</table>'
        return htmlPart

    def secondaryContentHTML():
        htmlPart = '<table cellpadding="0" cellspacing="0" border="0" width="' + str(widthRight) + '" style="width: ' + str(widthRight) + 'px; border-spacing: 0;">'
        htmlPart = htmlPart + '<tr><td>' + Renderer.rightSocialIcons() + '</td></tr>'
        for sb in NewsletterManager.currentNewsletter.secondaryContent:

            if sb.title not in ['', None, False]:
                htmlPart = htmlPart + '<tr><td height="32" valign="middle" style="width: 100%; height: 32px; padding: 0 9px; vertical-align: middle; background-color: #444444; font-size: 10.5pt;">'
                if sb.url not in ['', None, False]:
                    htmlPart = htmlPart + '<a href="' + sb.url + '" target="_blank" style="color: #eeeeee; text-decoration: none;">'
                    htmlPart = htmlPart + sb.title
                    htmlPart = htmlPart + '</a>'
                else:
                    htmlPart = htmlPart + sb.title
                htmlPart = htmlPart + '</td></tr>'

            if sb.style == "0":
                if sb.imageUrl not in ['', None, False]:
                    htmlPart = htmlPart + '<tr><td style="font-size: 10pt; padding: 4px 0 8px;">'
                    if sb.url not in ['', None, False]:
                        htmlPart = htmlPart + '<a href="' + sb.url + '" target="_blank" style="text-decoration: none;">'
                    htmlPart = htmlPart + '<img src="' + sb.imageUrl + '" border="0" align="left" height="100" width="100" style="width: 100px; height: 100px; padding: 0 7px 0 0; float: left;">'
                    if sb.url not in ['', None, False]:
                        htmlPart = htmlPart + '</a>'
                    htmlPart = htmlPart + sb.body
                    htmlPart = htmlPart + '</td></tr>'
                else:
                    htmlPart = htmlPart + '<tr><td style="padding: 7px 10px; font-size: 10pt;">'
                    htmlPart = htmlPart + sb.body
                    htmlPart = htmlPart + '</td></tr>'

            elif sb.style == "2":
                if sb.imageUrl not in ['', None, False]:
                    htmlPart = htmlPart + '<tr><td style="font-size: 10pt; padding: 0 0 8px;">'
                    if sb.url not in ['', None, False]:
                        htmlPart = htmlPart + '<a href="' + sb.url + '" target="_blank" style="text-decoration: none;">'
                    htmlPart = htmlPart + '<img src="' + sb.imageUrl + '" border="0" style="margin: 0 auto; display: block;">'
                    if sb.url not in ['', None, False]:
                        htmlPart = htmlPart + '</a>'
                    if sb.body not in ['', None, False]:
                        htmlPart = htmlPart + '<hr>'
                        htmlPart = htmlPart + sb.body
                    htmlPart = htmlPart + '</td></tr>'
                    
                    

        htmlPart = htmlPart + '</table>'
        return htmlPart

    def rightSocialIcons():
        htmlPart = '<table border="0" cellspacing="0" cellpadding="0" width="100%" style="border: none; padding: 0; margin: 8px 0 8px 0; width: 100%;">'
        htmlPart = htmlPart +   '<tr><td style="background-color: #3b5998;">'
        htmlPart = htmlPart +     '<a href="http://www.ics.com/l/yoxjivJK/Facebook+Icon" target="_blank" title="Check us out on Facebook!" style="display: block; text-align: center; text-decoration: none; height: 37px; background-color: #3b5998;">'
        htmlPart = htmlPart +        '<img src="http://www.ics.com/sites/all/themes/ics/images/social-icons/fb-img.png" border="0" style="border: none;">'
        htmlPart = htmlPart +      '</a>'
        htmlPart = htmlPart +    '</td>'
        htmlPart = htmlPart +    '<td style="background-color: #cd332d;">'
        htmlPart = htmlPart +      '<a href="http://www.ics.com/l/osyKBdZn/YouTube+Icon" target="_blank" title="Check out our YouTube channel!" style="display: block; text-align: center; text-decoration: none; height: 37px; background-color: #cd332d;">'
        htmlPart = htmlPart +        '<img src="http://www.ics.com/sites/all/themes/ics/images/social-icons/yt-img.png" border="0" style="border: none;">'
        htmlPart = htmlPart +      '</a>'
        htmlPart = htmlPart +    '</td>'
        htmlPart = htmlPart +    '<td style="background-color: #d64136;">'
        htmlPart = htmlPart +      '<a href="http://www.ics.com/l/toJTXh8B/Google+Icon" target="_blank" title="Check us out on Google+!" style="display: block; text-align: center; text-decoration: none; height: 37px; background-color: #d64136;">'
        htmlPart = htmlPart +        '<img src="http://www.ics.com/sites/all/themes/ics/images/social-icons/g-img.png" border="0" style="border: none;">'
        htmlPart = htmlPart +      '</a>'
        htmlPart = htmlPart +    '</td>'
        htmlPart = htmlPart +    '<td style="background-color: #00aced;">'
        htmlPart = htmlPart +      '<a href="http://www.ics.com/l/IAnBbKC0/Twitter+Icon" target="_blank" title="Follow our Twitter feed!" style="display: block; text-align: center; text-decoration: none; height: 37px; background-color: #00aced;">'
        htmlPart = htmlPart +        '<img src="http://www.ics.com/sites/all/themes/ics/images/social-icons/t-img.png" border="0" style="border: none;">'
        htmlPart = htmlPart +      '</a>'
        htmlPart = htmlPart +    '</td>'
        htmlPart = htmlPart +    '<td style="background-color: #e76a27;">'
        htmlPart = htmlPart +      '<a href="http://www.ics.com/l/Jn9uNKgx/RSS+Icon" target="_blank" title="Check out our Insight Blog RSS feed!" style="display: block; text-align: center; text-decoration: none; height: 37px; background-color: #e76a27;">'
        htmlPart = htmlPart +        '<img src="http://www.ics.com/sites/all/themes/ics/images/social-icons/rss-img.png" border="0" style="border: none;">'
        htmlPart = htmlPart +      '</a>'
        htmlPart = htmlPart +    '</td>'
        htmlPart = htmlPart +  '</tr>'
        htmlPart = htmlPart + '</table>'
        return htmlPart

    def emailUITest(newsletterHTML):
        toEmail = None
        try:
            if Renderer.arguments['action'][0] == 'email':
                newsletterText = "Please view the newsletter online at http://ics.com."
                toEmail = Renderer.arguments['email'][0]
                msg = MIMEMultipart('alternative')
                msg['Subject'] = NewsletterManager.currentNewsletter.fullTitle + " / ICS Newsletter (Test)"
                msg['From'] = "alex.balashov@gmail.com"
                msg['To'] = toEmail
                textVersion = MIMEText(newsletterText, 'text')
                htmlVersion = MIMEText(newsletterHTML, 'html')
                msg.attach(textVersion)
                msg.attach(htmlVersion)
                s = smtplib.SMTP_SSL('smtp.gmail.com', 465)
                s.login("alex@ics.com", "kerosine")
                #s.connect('localhost', 8300)
                s.sendmail("alex@ics.com", toEmail, msg.as_string())
                s.quit()
                print("<div class='message'>Successfully emailed " + toEmail + '</div>')
        except KeyError:
            pass
        except:
            print("Unable to send test email.")
            
        print('<div id="send-email">')
        print('<form name="test-email" action="render.py">')
        print('<label>Send a test e-mail to: </label>')
        print('<input type="text" name="email">')
        print('<input type="hidden" name="action" value="email">')
        print('<input type="hidden" name="newsletter" value="' + NewsletterManager.currentNewsletter.titleId + '">')
        print('<input type="submit" value="Email Newsletter">')
        print('</form></div>')

if __name__ == "__main__":
    Renderer.output()
