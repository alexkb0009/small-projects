import os, cgi
from NewsletterClasses import NewsletterManager, Newsletter

print('Status: 200 OK')
print('Content-type: text/html')
print('')

#NewsletterManager.resetAllData()
newsletterName = None
newsletterMode = None
newsletterIndex = None
query = os.environ.get('QUERY_STRING')
arguments = cgi.parse_qs(query)

NewsletterManager.fetchAllNewsletters()

if len(arguments) > 0:
    if 'mode' in arguments:            
        newsletterMode = arguments['mode'][0]
        if 'name' in arguments:
            if arguments['name'][0] != "none":
                NewsletterManager.currentNewsletterID = arguments['name'][0]
                for nl in NewsletterManager.newsletters:
                    if nl.titleId == arguments['name'][0]: # Found Newsletter
                        newsletterName = nl.fullTitle
                        NewsletterManager.currentNewsletter = nl
                        NewsletterManager.populateCurrentNewsletter()
                        newsletterIndex = NewsletterManager.newsletters.index(nl)
                        break
                else: newsletterName = arguments['name'][0]
            else:
                newsletterMode = None
                NewsletterManager.currentNewsletterID = None
        else:
            NewsletterManager.currentNewsletterID = None
 
    
print("""
<html>
<head>
<title>Newsletter Manager</title>
<style type='text/css' media='all'>
  @import url('newsletter-manager.css');
</style>""")
if newsletterMode == "edit":
    print('<script src="/files/ckeditor/ckeditor.js"></script>')
    print('<script>var nlData = {primary:[')
    for pi in NewsletterManager.currentNewsletter.primaryContent:
        print("{");
        print("uniqueID:'" + pi.uniqueID + "',")
        print("title:'" + pi.title + "',")
        print("imageUrl:'" + pi.imageUrl + "',")
        print("body:'" + pi.body + "',")
        print("url:'" + pi.url + "',")
        print("style:" + repr(pi.style) + ",")
        print("reusable:'" + repr(pi.reusable) + "'},")

    print('],secondary:[')
    for si in NewsletterManager.currentNewsletter.secondaryContent:
        print("{");
        print("uniqueID:'" + si.uniqueID + "',")
        print("title:'" + si.title + "',")
        print("imageUrl:'" + si.imageUrl + "',")
        print("body:'" + si.body + "',")
        print("url:'" + si.url + "',")
        print("style:" + repr(si.style) + ",")
        print("reusable:'" + repr(si.reusable) + "'},")
    print(']}</script>')

print("""
<script type="text/javascript" src="/files/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript" src="newsletter-edit.js"></script>
</head>
<body>
<div id="header">
  <a href="index.py">
    <h3 class="header-item">
      <span>ICS Inc. Newsletter Manager</span>
    </h3>
  </a>
""")

if NewsletterManager.currentNewsletterID != None:
    print('<a href="index.py?name=' + NewsletterManager.currentNewsletterID + '&mode=edit"><h4 class="header-item newsletter-name"><span>')
    print('<img src="./graphics/gnome_icons/24/document-page-setup.png" style="margin-bottom: -4px; margin-left: 4px;">' + newsletterName + '</span></h4></a>')

    print('<a href="#" id="top-save-button">')
    print('<h5 class="header-item"><span><img src="./graphics/gnome_icons/24/document-save.png">Save</span></h4>')
    print('</a>')

    print('<a class="view-link" href="render.py?newsletter=' + NewsletterManager.currentNewsletter.titleId + '">')
    print('<h5 class="header-item"><span><img src="./graphics/gnome_icons/24/document-print-preview.png">View</span></h4>')
    print('</a>')
elif newsletterMode == "new":
    print('<h4 class="header-item newsletter-name" style="color: #c00"><span>New [Unsaved]</span></h4>')
    
print("""
  <div class="header-item">
    <form name="newsletter-select" action="index.py" method="get">
      <label>Go to:</label>
      <select name="name">
        <option value="none" style="color: #bbb">None - Overview</option>
""")

for nl in NewsletterManager.newsletters:
    if NewsletterManager.currentNewsletterID == nl.titleId:
        print('<option value="' + nl.titleId + '" selected="true">' + nl.fullTitle + '</option>')
    else: print('<option value="' + nl.titleId + '">' + nl.fullTitle + '</option>')

print("""
      </select>
      <input type="hidden" name="mode" value="edit">
      <button type="submit">Edit</button>
    </form>
    <a href="index.py?mode=new">
      <img src="./graphics/gnome_icons/22/document-new.png" style="display: inline-block;margin-bottom: -6px;margin-top: -10px; margin-left: 5px;">
    </a>
  </div>
</div>
""")



if newsletterMode == None:
    print("""
    <div id="page-preface">
      Overview
    </div>
    """)

    print("""
    <div id="page-body" class="overview">
    <h4 style="margin-top: 3px;">Newsletters:</h4>
    <table>
    <tr><th>Edit</th><th>View</th><th>Unique ID</th></tr>""")

    for nl in NewsletterManager.newsletters:
        print('<tr>')
        print('<td class="edit-link"><a href="index.py?name=' + nl.titleId + '&mode=edit">')
        print('<img src="./graphics/gnome_icons/22/document-page-setup.png">edit</a></td>')
        print('<td class="view-link"><a href="render.py?newsletter=' + nl.titleId + '">')
        print('<img src="./graphics/gnome_icons/22/document-print-preview.png">' + nl.fullTitle + '</a></td>')
        #print('<td class="items-count">' + repr(len(nl.primaryContent) + len(nl.secondaryContent)) +  '</td>')
        print('<td class="uniqueId">' + nl.titleId + '</td></tr>')
    
    print("""
    </table>
    <div id="all-content-list">
    <h4>Re-usable Content Blocks:</h4>
    <table>
    <tr><th>Type</th><th>Title</th><th>In Newsletter</th><th>Unique ID</th></tr>""")
    
    for newsletter in NewsletterManager.newsletters:
        NewsletterManager.fetchReuseableBlocks("primary", newsletter)
        NewsletterManager.fetchReuseableBlocks("secondary", newsletter)
        for content in newsletter.primaryContent:
            #print('nl-' + newsletter.titleId + repr(newsletter.primaryContent[0].title))
            if content.reusable == True:
                print('<tr>')
                print('<td class="type">Primary</td>')

                print('<td class="title">' + content.title + '</td>')
                print('<td class="newsletter-title">' + content.parentNewsletter.fullTitle + '</td>')
                print('<td class="uniqueId">' + content.uniqueID + '</td></tr>')

        for content in newsletter.secondaryContent:
            if content.reusable == True:
                print('<tr>')
                print('<td class="type">Secondary</td>')

                print('<td class="title">' + content.title + '</td>')
                print('<td class="newsletter-title">' + content.parentNewsletter.fullTitle + '</td>')
                print('<td class="uniqueId">' + content.uniqueID + '</td></tr>')
    
    print("</table></div></div>")

if newsletterMode == "edit" and newsletterIndex != None:
    print('<div id="page-preface">')
    print('  Editing "' + newsletterName + '" &nbsp;&nbsp;&nbsp;')
    print('<div class="right" style="float: right">')
    print('<span style="">' + repr(len(NewsletterManager.currentNewsletter.primaryContent) + len(NewsletterManager.currentNewsletter.secondaryContent)) + ' Items</span>')
    
    print('</div></div>') 

    print('<div id="page-body" class="edit">')
    print('<form id="edit-newsletter-form" name="edit" method="POST" action="save.py">')
    print('<input type="hidden" name="newsletter" value="' + NewsletterManager.currentNewsletter.titleId + '">')
    print('<div id="newsletter-title-input" class="input centered-width"><label>Newsletter Title: </label>')
    print('<input type="text" name="newsletterTitle" value="' + NewsletterManager.currentNewsletter.fullTitle + '">')
    print('</div>') 
    
    print('<h3 class="section-header centered-width">Primary Content</h3><div id="primary-items">')


    print('</div><div class="add-new primary centered-width">Insert Primary Content</div>')
    
    print('<h3 style="margin-top: 27px;" class="section-header centered-width">Secondary Content</h3><div id="secondary-items">')
    print('</div><div class="add-new secondary centered-width">Insert Secondary Content</div>')
    print('<div class="centered-width save-area"><div id="save-button">Save</div></div>')
    print('<input type="hidden" id="postResponse" name="postResponse" value="view">')
    print('<input type="submit" id="save-button-actual" style="display: none;">')
    
    print('</form></div>')
    

elif newsletterMode == "new":
    print("""
    <div id="page-preface">
      New / Unsaved Newsletter
    </div>
    """)

    print('<div id="page-body" class="edit">')
    print('<form id="edit-newsletter-form" method="POST" action="save.py">')
    
    print('<div id="newsletter-title-input" class="input centered-width">')
    print('<input type="hidden" name="mode" value="new">')
    print('<input type="hidden" name="newsletter" value="NEW">')
    print('<label>Newsletter Title: </label>')
    print('<input type="text" name="newsletterTitle">')
    
    print('</div>')
    
    print('<h3 class="section-header centered-width">Primary Content</h3><div id="primary-items">')


    print('</div><div class="add-new primary centered-width">Insert Primary Content</div>')
    
    print('<h3 style="margin-top: 27px;" class="section-header centered-width">Secondary Content</h3><div id="secondary-items">')
    print('</div><div class="add-new secondary centered-width">Insert Secondary Content</div>')

    print('<div class="centered-width save-area"><div id="save-button">Save</div></div>')
    print('<input type="hidden" id="postResponse" name="postResponse" value="back">')
    print('<input type="submit" id="save-button-actual" style="display: none;">')
    print('</form></div>')


    
#print("<div height='100' width='100%'>")
#with open(os.path.dirname(os.path.realpath(__file__)) + '/index.py') as file:
#    output_file = open(os.path.dirname(os.path.realpath(__file__)) + '/index.py.source.txt', "w")
#    for line in file:
#        output_file.write(line)
#    output_file.close()
#print("</div>")

print("</body></html>")
