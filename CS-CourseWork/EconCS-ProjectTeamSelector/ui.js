﻿var data, ui;

$(document).ready(function () {

    data = {
        rolesCount: 0,
        peopleCount: 0
    }

    ui = {
        output: null,
        iterator: null,
        insertNewRole: function () {
            ui.output = '<div id="role-' + data.rolesCount + '" class="role-item n' + data.rolesCount + '">';
            ui.output += '<table cellspacing=0 cellpadding=0>'
            ui.output += '<tr class="row-1"><th class="col-1">&nbsp;</th><th class="col-2"># People</th><th class="col-3">Skill Required</th><th class="col-4">Min. Rating</th><th class="col-5">Skill Required</th><th class="col-6">Min. Rating</th><th class="col-7">Maximize Skill/Learning</th></tr>'

            ui.output += '<tr class="row-2"><td class="col-1"><div class="input-container">' + (data.rolesCount + 1) + '<input type="hidden" name="number" value="' + data.rolesCount + '"></div></td>';

            ui.output += '<td class="col-2"><div class="input-container">';
            ui.output += '<select class="people-requested" name="package[' + data.rolesCount + '][\'people\']">';
            ui.output += '<option value="1" selected="selected">1</option>';
            ui.iterator = 2
            while (ui.iterator < 11) {
                ui.output += '<option value="' + ui.iterator + '">' + ui.iterator + '</option>';
                ui.iterator++;
            }
            ui.output += '</select></div></td>';

            ui.output += '<td class="col-3"><div class="input-container"><select name="package[' + data.rolesCount + '][\'skill1\']" class="skill s1 n' + data.rolesCount + '" skill="1" role="' + data.rolesCount + '">';
            ui.output += '<option value="None">None</option>';
            // Make sure is populated w/ only Skills on this page.
            for (var key in jsData) {
                ui.output += '<option value="' + key + '">' + key + '</option>';
            }

            ui.output += '</select></div></td>';

            ui.output += '<td class="col-4"><div class="input-container"><select name="package[' + data.rolesCount + '][\'rating1\']" disabled class="rating-1 n' + data.rolesCount + '" role="' + data.rolesCount + '">';
            ui.output += '<option value="None" style="color: #888;">Select Skill</option>';
            ui.output += '</select></div></td>';

            ui.output += '<td class="col-5"><div class="input-container"><select name="package[' + data.rolesCount + '][\'skill2\']" class="skill s2 n' + data.rolesCount + '" skill="2" role="' + data.rolesCount + '">';
            ui.output += '<option value="None">None</option>';
            for (var key in jsData) {
                ui.output += '<option value="' + key + '">' + key + '</option>';
            }
            ui.output += '</select></div></td>';

            ui.output += '<td class="col-6"><div class="input-container"><select name="package[' + data.rolesCount + '][\'rating2\']" disabled class="rating-2 n' + data.rolesCount + '" role="' + data.rolesCount + '">';
            ui.output += '<option value="None">Select Skill</option>';
            ui.output += '</select></div></td>';

            ui.output += '<td class="col-7"><div class="input-container"><select name="package[' + data.rolesCount + '][\'maximize\']">';
            ui.output += '<option value="skill">Skill/Performance</option>';
            ui.output += '<option value="learning">Learning</option>';
            ui.output += '</select></div></td>';

            ui.output += '</tr></table><div class="x-button">x</div>';
            $("#request-area").append(ui.output);
            ui.output = null;
            data.rolesCount++;
            ui.updatePeopleCount();
        },
        updatePeopleCount: function () {
            data.peopleCount = 0;
            $(".people-requested").each(function () { data.peopleCount += parseInt($(this).val()) });
            $("#people-requested-count").html(data.peopleCount);
        },
        updateRatingsList: function () {
            ui.output = '';
            ui.iterator = 0;
            var maxRating = jsData[this.options[this.selectedIndex].value]
            while (ui.iterator <= maxRating) {
                ui.output += '<option value="' + ui.iterator + '">' + ui.iterator + '</option>';
                ui.iterator++;
            }
            $('select.n' + $(this).attr('role') + '.rating-' + $(this).attr('skill')).removeProp("disabled").html(ui.output);
            ui.output = null;
        },
        removeRole: function () {
            $("#request-area #role-" + (data.rolesCount-- - 1)).remove();
            ui.updatePeopleCount();
        },
        runAuction: function () {
            $("#results-container").html("<h4 style='text-align:center'>Working on server - this might take a little bit.</h4>");
            $("#results-pane-container").html("");
            jQuery.ajax({
                url: 'DoAuction.py',
                method: 'POST',
                data: $('#project-request-form').serialize()
            }).done(function (response) {
                $("#results-container").html(response);
                $("#results-pane-container").html($("#results-pane"));
            }).fail(function () {
                alert("Oops");
            });
        }
    }

    // UI Bindings
    $("#add-employee-button").click(ui.insertNewRole);
    $("#request-area").on('change', "select.people-requested", ui.updatePeopleCount);
    $("#request-area").on('change', "select.skill", ui.updateRatingsList);
    $("#request-area").on('click', "div.x-button", ui.removeRole);
    $("#run-button").click(ui.runAuction);

});