import collections

class ui(object):
    """description of class"""

    def createHead(pageTitle, css = None, js = None, jsData = None):
        html = '<head>\n'
        html += '  <title>' + pageTitle + ' | Combinatorial Auction Team Project Allocation (Simulation)</title>\n'
        html += '  <link rel="stylesheet" href="global.css" type="text/css" media="all">\n'
        if js != None:
            if 'jQuery' in js:
                html += '  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>\n'
            if 'ui' in js:
                html += '  <script src="./ui.js"></script>\n'

        if jsData != None:
            html += '  <script>\n'
            html += 'var jsData={'
            if isinstance(jsData,dict):
                html += ui.createJSData(jsData)
            elif jsData is list:
                for item in jsData:
                    html += jsData.index(item) + ': {'
                    ui.createJSData(item)
                    html += '}'
                    if jsData.index(item) < len(jsData) - 1:
                        html += ','

            html += '};\n  </script>\n'

        html += '</head>\n'
        return html

    def createJSData(inputDict: dict):
        output = ''
        pyDict = collections.OrderedDict(reversed(sorted(inputDict.items()))) # replace w inputDict.copy() to randomize
        while len(pyDict.keys()) > 0:
            (key, value) = pyDict.popitem()
            output += "'" + str(key) + "': "
            if isinstance(value, str): output += "'" + value + "'"
            else: output += str(value)
            if len(pyDict.keys()) > 0:
                output += ','

        return output

    class form():
        """form object"""
        def createInput(name,type,classes = None, value = None, otherAttributes = None):
            output = ''
            if type == 'textarea': output += '<textarea '
            elif type == 'select': output += '<select '
            else: 
                output += '<input type="' + type + '" '
            output += 'name="' + name + '" id="' + '" ' 
            if classes is list:
                output += 'class="'
                for item in list:
                    output += item + ' '
                output += '" '
            output += '>'

            if isinstance(value, str):
                output += value

            if type == 'textarea': output += '</textarea>'
            elif type == 'select': output += '</select>'
            return output

