
class DataSource():

    def getSkills(): # Use dummy data for now
        return DataSource.allowableSkillsDummyData 

    def getAvailableEmployees():
        return DataSource.usersDummyData

    def getUsersCount():
        return len(DataSource.usersDummyData.keys())

                             ##     Skill           : Max Rating
    allowableSkillsDummyData = { 
                                    '.NET'          : 3, # 'Sub-Skills' might have less range to allow succinct dif in rating.
                                    'Algorithms'    : 3, # Experience in optimizing, reducing complexity, data structs, etc.
                                    'Android'       : 5, # From coding/compiling w/ Android SDK to modifying the OS (custom ROMs, etc.).
                                    'C++'           : 5, # All-Around C++ 
                                    'C#'            : 5, # All-around C#, etc.
                                    'CSS'           : 5, # From basic content editing to media queries, LESS, etc.
                                    'DirectX'       : 4,
                                    'Drupal'        : 5, # From editing content to writing modules.
                                    'Graphic Design': 5, # Ability to make PhotoShop/Illustrator sing, etc. 
                                    'HTML(5)'       : 8, # Rating 1-3 for basic HTML/content edit, 4+ for HTML5/WebDev 
                                    'Java'          : 6, # Covering beginners to enterprise level
                                    'JavaScript'    : 5, 
                                    'Marketing'     : 5, # Ability to come up with successful campaigns.
                                    'Objective-C'   : 5,
                                    'OpenGL'        : 4, 
                                    'PHP'           : 5,
                                    'Python'        : 5,
                                    'QML'           : 5,
                                    'Qt'            : 5,
                                    'Ruby'          : 5,
                                    'Sales'         : 5, # Ability to close deals?
                                    'UX Design'     : 3, # Ability to come up with good layout, information architecture, experience.
                                    'WebGL'         : 3,
                                    'Web Services'  : 3, # Ability to create JSON/XML Rest APIs for access by other apps, etc.
                                    'XAML'          : 3                              
                                    
                                }
                                
    usersDummyData = {
                        'bill123' : {
                                    'name'      : 'Bill',
                                    'birthdate' : '1972-04-01', ## To use for "Age" compatibility. Not valued currently.
                                    'ipAddress' : '127.0.0.01', ## To use w/ geolocation API to obtain long/lat coords. Would be obtained thru access logs.
                                    'skills'    : {
                                                    'C#'        : 4,
                                                    'C++'       : 3,
                                                    'Java'      : 5,
                                                    'Android'   : 4,
                                                    '.NET'      : 2,
                                                    'XAML'      : 1
                                                   },
                                    'interests' : ['Food', 'Burgers', 'Programming', 'Backend', 'Java'],
                                    'communicationPreference' : 'Skype'
                                  },

                        'alexb' : {
                                    'name'      : 'Alex',
                                    'birthdate' : '1990-03-22',
                                    'ipAddress' : '24.63.27.50',
                                    'skills'    : {
                                                    'C#'        : 3,
                                                    'C++'       : 0,
                                                    'QML'       : 2,
                                                    'Java'      : 2,
                                                    '.NET'      : 2,
                                                    'CSS'       : 5,
                                                    'HTML(5)'   : 7,
                                                    'Drupal'    : 4,
                                                    'Android'   : 1,
                                                    'Graphic Design' : 4,
                                                    'Algorithms' : 2,
                                                    'JavaScript': 5,
                                                    'Python'    : 3,
                                                    'PHP'       : 4,
                                                    'UX Design' : 2,
                                                    'Web Services' : 1,
                                                    'XAML'      : 2
                                                   },
                                    'interests' : ['Food', 'Kayaking', 'Hiking', 'Programming', 'Burgers', 'Web Development', 'JavaScript'],
                                    'communicationPreference' : 'Skype'
                                  },
                        'sam123' : {
                                    'name'      : 'Sam',
                                    'birthdate' : '1987-09-20',
                                    'skills'    : {
                                                    'Drupal'    : 5,
                                                    'PHP'       : 5,
                                                    'Python'    : 4,
                                                    'Ruby'      : 3,
                                                    'UX Design' : 2,
                                                    'Graphic Design' : 3,
                                                    'CSS'       : 4,
                                                    'HTML(5)'   : 6
                                                   },
                                    'interests' : ['Hiking', 'Kayaking', 'Canoeing', 'JavaScript', 'Python'],
                                    'communicationPreference' : 'Skype' 

                                 },
                        'joe345' : {
                                    'name'      : 'Joe',
                                    'birthdate' : '1987-09-20',
                                    'skills'    : {
                                                    'Java'      : 5,
                                                    'C++'       : 5,
                                                    'PHP'       : 3,
                                                    'Ruby'      : 3,
                                                    'HTML(5)'   : 4,
                                                    'Android'   : 4,
                                                    'Algorithms': 4,
                                                    'Objective-C': 3
                                                   },
                                    'interests' : ['Food', 'Burgers', 'Programming', 'Java'],
                                    'communicationPreference' : 'EMail' 

                                 },

                        'joe678' : {
                                    'name'      : 'Joe Two',
                                    'birthdate' : '1981-09-20',
                                    'skills'    : {
                                                    'Java'      : 5,
                                                    'C++'       : 5,
                                                    'PHP'       : 3,
                                                    'Ruby'      : 3,
                                                    'HTML(5)'   : 4,
                                                    'Android'   : 4,
                                                    'Algorithms': 4,
                                                    'Objective-C': 3
                                                   },
                                    'interests' : ['Food', 'Burgers', 'Programming', 'Java'],
                                    'communicationPreference' : 'EMail' 

                                 },

                        'roger' : {
                                    'name'      : 'Roger',
                                    'birthdate' : '1981-09-20',
                                    'skills'    : {
                                                    'Java'      : 3,
                                                    'C++'       : 5,
                                                    'PHP'       : 2,
                                                    'HTML(5)'   : 5,
                                                    'Qt'        : 4,
                                                    'QML'       : 3,
                                                    'JavaScript': 3,
                                                    'Python'    : 3,
                                                    'OpenGL'    : 4,
                                                    'WebGL'     : 3
                                                   },
                                    'interests' : ['Food', 'Burgers', 'Programming', 'Java'],
                                    'communicationPreference' : 'EMail' 

                                 },

                        'rachel2' : {
                                    'name'      : 'Rachel',
                                    'birthdate' : '1983-09-20',
                                    'skills'    : {
                                                    'Marketing' : 4,
                                                    'UX Design' : 4,
                                                    'Graphic Design': 3,
                                                    'Sales'     : 2,
                                                    'HTML(5)'   : 2
                                                   },
                                    'interests' : ['Design', 'HTML', 'Hiking', 'Coding'],
                                    'communicationPreference' : 'EMail' 
                                 },

                        'roger2' : {
                                    'name'      : 'Roger Two',
                                    'birthdate' : '1981-09-20',
                                    'skills'    : {
                                                    'Java'      : 3,
                                                    'C++'       : 5,
                                                    'PHP'       : 2,
                                                    'HTML(5)'   : 5,
                                                    'Qt'        : 4,
                                                    'QML'       : 3,
                                                    'JavaScript': 3,
                                                    'Python'    : 3,
                                                    'OpenGL'    : 4,
                                                    'WebGL'     : 3
                                                   },
                                    'interests' : ['Food', 'Burgers', 'Programming', 'Java'],
                                    'communicationPreference' : 'EMail' 

                                 },

                        'roger3' : {
                                    'name'      : 'Roger Three',
                                    'birthdate' : '1981-09-20',
                                    'skills'    : {
                                                    'Java'      : 3,
                                                    'C++'       : 5,
                                                    'PHP'       : 2,
                                                    'HTML(5)'   : 5,
                                                    'Qt'        : 4,
                                                    'QML'       : 3,
                                                    'JavaScript': 3,
                                                    'Python'    : 3,
                                                    'OpenGL'    : 4,
                                                    'WebGL'     : 3
                                                   },
                                    'interests' : ['Rock Music', 'Movies', 'Python', 'JavaScript'],
                                    'communicationPreference' : 'Skype' 

                                 },
                      }

                      