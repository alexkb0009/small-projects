import os, cgi
from UserInterface import ui
from DataSource import DataSource

print('Status: 200 OK')
print('Content-type: text/html')
print('')

opts = {
           'title' : "Home"
       }

print('<html>\n' + ui.createHead(opts['title'], js = ['jQuery', 'ui'], jsData = DataSource.getSkills()) + '<body>')
print('''
<div id="top-header">
  <h4>Welcome to the Project Team Requester Interface</h4>
  <div id="right-header-block">
    Alexander Balashov 
  </div>
</div>
''')

print('''
<div id="page-area">
  <div class="intro">
    
      <small>Class project for CSCI E-186 <a href="http://www.seas.harvard.edu/courses/cs186/E186-syllabus.html">Economics and Computation</a>, Spring 2014 at Harvard University.</small>
      <hr style="height: 0; border: none; border-top: 1px solid #bbb;">
      This prototype was created in 2 weeks. &nbsp;&nbsp; \\\ &nbsp;&nbsp;&nbsp; Fails if more people specified than are available, or not enough available for role/ratings specified.
      <hr style="height: 0; border: none; border-top: 1px solid #bbb;">
      This simulation runs a combinatorial auction in order to select the best set of 'employees' out of all possible combinations which match requirements, with the goal of creating the best project team. Uses weighted components for calculating a valuation to assign to each team, which serves as basis for ranking.
    
  </div>
  <form method="POST" name="request-form" id="project-request-form">
  <div class="heading">Project Criteria</div>
  <div id="project-details">
    <table cellspacing="0" cellpadding="0">
      <tr class="row-1">
        <th class="col-1">Project Name or Description</th>
        <th class="col-2">Keywords/Tags (compared against interests)</th>
        <th class="col-3"></th>
        <th class="col-4 people-requested-label">People Requested</th>
      </tr>
      <tr class="row-2">
''')

print('<td class="col-1"><div class="input-container">' + ui.form.createInput('project-name','textarea', value='') + '</div></td>')
print('<td class="col-2"><div class="input-container">' + ui.form.createInput('keywords','textarea', value = 'Separate, With, Commas') + '</div></td>')
print('<td class="col-3"><div class="input-container">' + '</div></td>')
print('<td class="col-4"><div id="people-requested-count">0</div><span id="num-available" title="# of People Available">/' + str(DataSource.getUsersCount()) + '</span></td>')

print('''
      </tr>
    </table>
  </div>
  <div class="heading">I need...</div>
  <div id="request-area">

  </div>

  <div id="add-employee-button" class="ui-project-button">Add Project Role(s)</div>

</form>
<div id="run-button">Run Auction</div>
<div id="results-pane-container"></div>
<div id="results-container"></div>
''')
print('</div>')
print('</body></html>')
