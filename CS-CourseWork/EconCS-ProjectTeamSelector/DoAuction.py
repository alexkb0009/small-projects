import os, cgi, itertools
from DataSource import DataSource

def debug(msg):
    on = True
    if on:
        print("<br><span class='debug'><span class='debug-label'>Log: </span>" + str(msg) + "</span>")
    return msg

print('Status: 200 OK')
print('Content-type: text/html')
print('')

print('<h3 class="results-header">Computation Log</h3>')

query = os.environ.get('QUERY_STRING')
form = cgi.FieldStorage()

exitFlag = False

projectDetails = {
            'name'      : form.getfirst('project-name'),
            'keywords'  : list(map(lambda k: k.lower().strip(), form.getfirst('keywords').split(',')))
          }

allSkills = DataSource.getSkills()
roleIndices = form.getlist('number')
availableEmployees = None
packages = list()

def getCriteria(roleId:int):
    return {
                'skill-1': form.getfirst("package[" + str(roleId) + "]['skill1']"),
                'skill-1-rating': form.getfirst("package[" + str(roleId) + "]['rating1']"),
                'skill-2': form.getfirst("package[" + str(roleId) + "]['skill2']"),
                'skill-2-rating': form.getfirst("package[" + str(roleId) + "]['rating2']")
            }

def findSuitableEmployees(indx:int):
    
    criteria = getCriteria(indx)

    global availableEmployees
    availableEmployees = DataSource.getAvailableEmployees()

    employees = list()
    for e in availableEmployees:
        if ((criteria['skill-1'] in availableEmployees[e]['skills'] and int(criteria['skill-1-rating']) <= availableEmployees[e]['skills'][criteria['skill-1']]) or criteria['skill-1'] == 'None') and ((criteria['skill-2'] in availableEmployees[e]['skills'] and int(criteria['skill-2-rating']) <= availableEmployees[e]['skills'][criteria['skill-2']]) or criteria['skill-2'] == 'None'):
            employees.append(e)

    if len(employees) == 0: return False
    print('<div class="availpeople-heading">' + str(len(employees)) + " People available for role " + str(int(indx) + 1) + '</div>')
    for e in employees:
        print('<div class="availperson">' + e)
        if criteria['skill-1'] != 'None' or criteria['skill-2'] != 'None':
            print('(')
            if criteria['skill-1'] != 'None':
                print(criteria['skill-1'] + ': ' + repr(availableEmployees[e]['skills'][criteria['skill-1']]))
            if criteria['skill-1'] != 'None' and criteria['skill-2'] != 'None':
                print(', ')
            if criteria['skill-2'] != 'None':
                print(criteria['skill-2'] + ': ' + repr(availableEmployees[e]['skills'][criteria['skill-2']]))
            print(')')
        print('</div>')
    return employees

###
### Function For Generating All Possible Packages
### Using a multi-dimensional counter (employeeIndices) to count through suitable employees (list of usernames or tuples)
###

def makePackages():
    suitableEmployees = list()
    numberForRole = list()
    employeeIndices = list()

    for i in roleIndices: 
        suitableEmployees.append(findSuitableEmployees(i))
        if suitableEmployees[int(i)] == False: return False
        numberForRole.append(int(form["package[" + str(i) + "]['people']"].value))
        employeeIndices.append(0)

        if numberForRole[int(i)] > 1:
            suitableEmployees[int(i)] = list(itertools.combinations(suitableEmployees[int(i)], numberForRole[int(i)]))
        elif numberForRole[int(i)] == 0:
            global exitFlag
            exitFlag = True
            print('<span class="exit-message error">Exiting - No Employees Requested For Role ' + str(int(i) + 1) + '</span>')
            return False


    def makeAPackage():
        

        def increaseSlot(slot, onlyOne = False):

            if slot < 0: 
                debug("Slot is below 0, exiting.")
                global exitFlag
                exitFlag = True                                         # IMPORTANT: Quit the while() loop which makes packages
                return # done!


            employeeIndices[slot] = employeeIndices[slot] + 1

            if slot < len(employeeIndices) - 1:                         # If a higher slot, reset all below
                for r in range(slot + 1, len(employeeIndices)):
                     #debug('resetting beyond ' + repr(slot) + ":" + r)
                     employeeIndices[r] = 0



            if employeeIndices[slot] >= len(suitableEmployees[slot]):   # Oops, reached end of list
                if onlyOne: 
                    return False                                        # Return error or something if doing only once for one slot.
                else:       
                    #employeeIndices[slot] = 0                                            
                    return increaseSlot(slot - 1)                       # Otherwise incr. next slot up.
            else:
                return True                                             # Yay, not @ end of avail employee list for current slot

        def checkDoubles(pkg, slot):        
            foundAny = False 

            #debug("Checking if (slot " + str(slot) +  ") index " + repr([employeeIndices[slot]]) + " is in " + repr(pkg))
            if suitableEmployees[slot][employeeIndices[slot]] in pkg: foundAny = True       # Check 1 : If employee in pkg
            if isinstance(suitableEmployees[slot][employeeIndices[slot]], str):     
                for e in pkg:
                    if isinstance(e, tuple):
                        if suitableEmployees[slot][employeeIndices[slot]] in e: 
                            foundAny = True
                        
            elif isinstance(suitableEmployees[slot][employeeIndices[slot]], tuple):         # If is a tuple of employees
                for employee in suitableEmployees[slot][employeeIndices[slot]]:
                    for e in pkg:
                        if employee == e: foundAny = True
                        if isinstance(e, tuple):
                            if employee in e: foundAny = True

            else: 
                debug("Wrong Type")
                return False                # Wrong type
            if not foundAny:                # No Doubles Found (Yay!)
                return True   
            else:                           # Found a double, increase slot, try again.
                if not increaseSlot(slot, True):   
                    #debug("Not enough suitable employees for roles specified. @ role " + str(slot))
                    return False
                return checkDoubles(pkg, slot)
        
        ##    
        ## Lets get to creating this package
        ##

        package = list()

        for roleIndex in range(len(employeeIndices)):

            if checkDoubles(package, roleIndex):
                package.append(suitableEmployees[roleIndex][employeeIndices[roleIndex]])
                
            else: 
                increaseSlot(roleIndex)
                return False

        increaseSlot(len(employeeIndices) - 1)
        packages.append(package)
        return package

        ##
        ##
        ##


    #for i in range(len(suitableEmployees)):
    #    if len(suitableEmployees[i]) > numberForRole[i]:
    #        return False
    #if sum(numberForRole) > sum(suitableEmployees): return False



    ##
    ##  Generating Packages
    ##

    n = 0
    print('<br>');
    while exitFlag == False and n < 10000:                                  # Limit number of cycles/attempts; prevent infinite loop
        pkg = makeAPackage()
        debug("Created Package " + repr(n) + ": " + repr(pkg))
        n = n + 1

    ##
    ##  Some Package Results/Stats
    ##

    debug("<br><br>Employee Indices (current): <b>" + repr(employeeIndices) + '</b>')
    debug("<b>Length of lists:</b>" )
    for i in suitableEmployees:
        debug("&nbsp;&nbsp;&nbsp;" + repr(len(i)))
    debug("Generated <b>" + repr(len(packages)) + "</b> total packages<br>")
    return True                                                             # Generated packages and put them into 'packages'


###                    ###
### Valuation Function ###
###                    ###

def valuation(package, extendedResult = False):

    ##
    ##  Merge tuples into package list

    def mergeTuplesPackages(pkg):
        n = 0
        end = len(package)
        mergedPackage = list()
        while n < end:
            if isinstance(package[n],tuple):
                for itm in package[n]:
                    mergedPackage.append((itm, n))          # Tuple: (name, role id)
            else: mergedPackage.append((package[n], n))
            n = n + 1
        return mergedPackage

    package = mergeTuplesPackages(package)

    storedList = list()
    def store(component, value):
        if extendedResult:
            storedList.append((component,value))
        return value

    ##
    ## Valuation Functions

    def employeeOverProjectInterests(e):
        overlapInterests = 0
        for keyword in projectDetails['keywords']:  # Already went through .strip() and .lower().
            if keyword in map(lambda interest: interest.strip().lower(), availableEmployees[e[0]]['interests']):
                overlapInterests += 1
        return (overlapInterests * 10) / (len(projectDetails['keywords']) * 8 + len(availableEmployees[e[0]]['interests']) * 2 ) 

    def employeeRatingOverRequired(e):
        maxPref = form.getfirst("package[" + str(e[1]) + "]['maximize']")
        criteria = getCriteria(e[1])
        sumReqd = 0
        sumHas = 0
        sumRange = 0
        if criteria['skill-1-rating'] != None:
            sumReqd += int(criteria['skill-1-rating'])
            sumHas += int(availableEmployees[e[0]]['skills'][criteria['skill-1']])
            sumRange += (allSkills[criteria['skill-1']] - int(criteria['skill-1-rating']))
        if criteria['skill-2-rating'] != None: 
            sumReqd += int(criteria['skill-2-rating'])
            sumHas += int(availableEmployees[e[0]]['skills'][criteria['skill-2']])
            sumRange += (allSkills[criteria['skill-2']] - int(criteria['skill-2-rating']))
        if sumRange == 0: sumRange = 1
        if criteria['skill-1-rating'] == None and criteria['skill-2-rating'] == None:
            return 0.0
        elif maxPref == 'learning':
            debug(str(sumHas) + '..' + str(sumReqd) + '..' + str(sumRange))
            return float(1 / ((sumHas - sumReqd) / sumRange + 1)) - 0.5
        else:
            return float((sumHas - sumReqd) / sumRange)

    def sharedInterests(pkg):
        mergedInterests = list()
        totalInterests = 0
        for e in pkg:
            mergedInterests = list(set(mergedInterests + availableEmployees[e[0]]['interests']))
            totalInterests = totalInterests + len(availableEmployees[e[0]]['interests'])
        return 1 - len(mergedInterests)/totalInterests

    def diversityOfSkills(pkg):
        mergedSkills = list()
        for e in pkg:
            for skill in availableEmployees[e[0]]['skills']:
                if skill not in mergedSkills and availableEmployees[e[0]]['skills'][skill] > 1: # Add skills of employees in package where skill rating > 1
                    mergedSkills.append(skill)
        return len(mergedSkills) / len(allSkills)

    def communicationPreference(pkg):
        cpList = list()
        for e in pkg:
            cpList.append(availableEmployees[e[0]]['communicationPreference'].strip().lower().replace('-',''))

        return max(map(lambda x: cpList.count(x), set(cpList))) / len(cpList)
        

    totalValue = 0
    # Individual Components
    for employee in package:
        totalValue = totalValue + (33/100) * store('Interest vs Project [' + employee[0] + ']', employeeOverProjectInterests(employee))
        totalValue = totalValue + (20/100) * store('Rating over Required [' + employee[0] + ']', employeeRatingOverRequired(employee))
    totalValue = totalValue / len(package) # Get average for package

    # Shared Components
    totalValue = totalValue + (12/100) * store('Shared Interests', sharedInterests(package))
    totalValue = totalValue + (8/100) * store('Diversity of Skills', diversityOfSkills(package))
    totalValue = totalValue + (7/100) * store('Communication Preference', communicationPreference(package))
    #ToDo: Geographic Proximity 

    
    if (extendedResult):
        # Get some result info
        output  = '<div class="package-valuation">'
        output += 'PACKAGE <b>VALUATION</b><br>'
        for e in package:
            output += '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '
            output += '<small>Role</small> ' + str(e[1] + 1) + ': <b>' + e[0] + '</b>'
            output += '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '
            output += '&nbsp;&nbsp;<small class="interests repr"><b>interests</b>: ' + str(availableEmployees[e[0]]['interests']) + '</small>'
            output += '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '
            output += '&nbsp;&nbsp;<small class="skills repr"><b>skills</b>: ' + str(availableEmployees[e[0]]['skills']) + '</small>'
            #print(str(list(map(lambda p: ('Role ' + str(p[1] + 1), p[0]), package))) )
        output += ('<br><br><div class="total-value-container"><span class="total-value-label">Total Value:</span> <span class="total-value">' + str(totalValue) + '</span></div>')
        output += ('<div class="component-valuation-container"><b>Component Valuations</b>')
        for t in storedList:
            output += ('<br><span class="c-value label">' + t[0] + ':</span> <span class="c-value value">' + str(t[1]) + '</span>') # list all values on which store() was called earlier
        output += ('</div></div>')

        return (totalValue, output)

    return totalValue

def findWinningPackage(onlyWinner = True):
    if onlyWinner:
        return max(packages, key= lambda pkg: valuation(pkg))
    else: 
        winners = list()
        i = 0
        maxI = min(10, len(packages))
        while i < maxI:
            currWinner = max(packages, key= valuation)
            winners.append((currWinner, valuation(currWinner)))
            packages.remove(currWinner)
            i = i + 1
        return winners #sort(packages, key= lambda pkg: valuation(pkg))



######              ######                      ######
  ######              ######                      ######
    ######  INITIALIZE  ######  AND RETURN RESULTS  ######
  ######              ######                      ######
######              ######                      ######

##
## Puts packages into global 'packages' list. Alternatively could return list, but not important enough to change ATM.

makePackages() 

        ##
        ## findWinningPackage(makePackages()) might look more elegant but.. might change later. :) Wanted a global packages for some reason I forgot why now

winners = findWinningPackage(False)       # Winner/Scoring Mechanism 
resultsPane  = '<div id="results-pane">'
if len(winners) > 0:
    resultsPane += '<h3 class="results-header">Top ' + str(len(winners)) + ' Results</h3>'
    resultsPane += '<small>Packages:</small>'
    
    for w in winners:
        resultsPane += '<div class="package-winner"><small class="total-value">' + str(w[1]) + '</small>' + str(w[0]) + '</div>'

    resultsPane += '<h3 class="results-header">Valuations</h3>'

    for w in winners:
        resultsPane += valuation(w[0], True)[1]
else: 
    resultsPane += '<h3 class="results-header">No results found for criteria provided.</h3>'

resultsPane += '</div>'

print(resultsPane)

        
##
##
## Return results


