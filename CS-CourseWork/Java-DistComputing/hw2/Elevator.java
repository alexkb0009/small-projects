package cscie160.hw2;

/**
 * Elevator. <br>
 * Homework assignment 2.<br>
 * Edited assignment one to better fit HW1 specs per proper HW1 solution, then building on top of for HW2 specs.<br>
 * Doing best to follow proper coding/management/pattern procedures, especially as suggested in class materials.
 *
 * @author Alexander Balashov
 * @version 0.0.2
 * @see Floor
 */
 
public class Elevator {

  private static final int DEFAULT_FLOOR = 1;
  private static final int MAXCAP = 10;
  private static final int MAXFLOOR = 8; // Works with any number. 
  private int travelDirection = 0;
  private static final int STILL = 0;
  private static final int UP = 1;
  private static final int DOWN = 2;
  private int moveCount; // Curious on what movement its on.
  private int currentFloor; // Previously default val was 1, moved to initializer.
  
  private Floor[] floors = new Floor[MAXFLOOR + 1];
  
  private boolean[] destinationRequest = new boolean[MAXFLOOR + 1];  // Changed per HW1 solution.
  private int[] numOfPassengersForFloor = new int[MAXFLOOR + 1]; // Added +1 to have relation of key:floor.
  
  /**
   * Elevator class constructor method. Based somewhat off proper HW1 solution.
   */  
 
  Elevator(){
    this.currentFloor = 1;
    System.out.println("\nINITIALIZED. Status:   \n" + this + "\n");
	for (int i=1; i <= MAXFLOOR; i++) {
	  numOfPassengersForFloor[i] = 0;
	  destinationRequest[i] = false;
	  floors[i] = new Floor(i);
    }
	this.moveCount = 0;
  }
  
  /**
   * The toString() method override. Returns a sentence describing what the elevator is doing.
   */
  
  public String toString(){
    return "Elevator at floor " + this.currentFloor + (this.travelDirection == 0 ? " standing still" : (" moving " + this.GetDirectionString())) + " w/ " + GetNumberOfPassengers() + " passengers.";
  } 
  
  /**
   * Informed via HW1 solution and lecture's advice on keeping data private, - to utilize get/set methods.
   * Using capital first letter as am used to it from C#.
   * 
   * @return The current floor elevator is on.
   */

  public int GetCurrentFloor() {
	return currentFloor;
  }
	
  private void SetCurrentFloor(int currentFloor) {
	this.currentFloor = currentFloor;
  }
  
  public int GetMaxFloor() {
	return MAXFLOOR;
  }
  
  public int GetDirectionOfTravel() {
	return travelDirection;
  }
	
  private void SetDirectionOfTravel(int travelDirection) {
	this.travelDirection = travelDirection;
  }
  
  /**
   * Get string value of travelDirection.
   *
   * @return Direction of travel (string).
   */
  
  public String GetDirectionString(){
  	if (this.travelDirection == 0) return "nowhere";
    if (this.travelDirection == 1) return "upwards";
	if (this.travelDirection == 2) return "downwards";
	return "Error: ";
  }
  
  public String GetDirectionString(int td){
  	if (td == 0) return "nowhere";
    if (td == 1) return "upwards";
	if (td == 2) return "downwards";
	return "Error: ";
  }
  
  /**
   * Gets # of passengers in total on the elevator, based off the array which says how many are
   * destined per floor.
   *
   * @return Number of passengers in elevator cabin.
   */
  
  public int GetNumberOfPassengers(){
    int passengers = 0;
    for (int i=1; i <= MAXFLOOR; i++) {
	  passengers += GetNumberOfPassengersForFloor(i); 
	}
	return passengers;
  }
  
 /**
  * @param floor : Floor Number.
  * @return Number of passengers in elevator cabin.
  */  
  
  public int GetNumberOfPassengersForFloor(int floor){
    return numOfPassengersForFloor[floor];
  }
  
  public void ClearNumberOfPassengersForFloor(int floor){
    numOfPassengersForFloor[floor] = 0;
  }
  
 /**
  * @param floor : Floor Number.
  * @return If requested floor has passengers waiting (bool).
  */  
  
  public boolean IsFloorRequested(int floor){
    return destinationRequest[floor];
  }
  
  
  /** 
   * <b>BELOW JAVADOC TAKEN FROM PROPER HW1 SOLUTION. </b> Re-implementing for HW2.
   * <p>
   * Moves the elevator UP or DOWN depending on the following algorithm: 
   * If the current direction of travel is UP and the current floor is not the top floor, move the elevator UP one floor. <br />
   * If the current direction of travel is UP and current floor is topmost floor, move elevator down and set the direction of travel to DOWN. <br />
   * If the direction of travel is down and current floor is not ground floor, move elevator DOWN. <br />
   * If direction of travel is down and current floor is ground floor, change direction of travel to UP and move up. </p>
   * <p>
   * Moved the reversal of elevator direction to directly after attaining a boundary floor in order to obtain more accurate
   * readouts (e.g. Moving upwards at floor 1 vs downwards). 
   */
	 
  public void move(){
    int travelDir = GetDirectionOfTravel();
	int floor = GetCurrentFloor();
	try {
	  if (travelDir == UP){
	    if (floor < MAXFLOOR) {
		  floor++;
		  if (floor == MAXFLOOR) SetDirectionOfTravel(DOWN);
		//} else if (floor == MAXFLOOR) {
		//  floor--;
		//  SetDirectionOfTravel(DOWN);
		}
	  } else if (travelDir == DOWN){
	    if (floor > DEFAULT_FLOOR) {
		  floor--;
		  if (floor == DEFAULT_FLOOR) SetDirectionOfTravel(UP);
		//} else if (floor == DEFAULT_FLOOR) {
		//  floor++; 
		//  SetDirectionOfTravel(UP);
		}
	  }
	} catch (Exception ex) { 
	  new Exception("Floor " + floor + " is outside the building bounds or not assigned directionality.");
	  return;
    }
	this.SetCurrentFloor(floor);
	this.moveCount++;
  }
  
  /**
   * <b>BELOW JAVADOC TAKEN FROM PROPER HW1 SOLUTION. </b> Re-implementing for HW2.
   * <p>
   * Will perform the following actions:
   * Stop the motion of elevator. <br/>
   * Disembark the passengers for the current floor.<br/>
   * Clears current floor as a destination request.
   * <p>
   * For HW2, added method of Floor to take passengers destined for it, and add to its count of people
   * on floor. 
   */
  
  public void stop(){
  	int floor = GetCurrentFloor(); 
    System.out.println("Stopping on floor " + floor + ". Passengers waiting here: " + floors[floor].GetPassengers() + " (Move " + this.moveCount + ")"); 
	destinationRequest[floor] = false;
	floors[floor].UnloadPassengers(this, numOfPassengersForFloor[floor]); // Unloads passengers.
    /* numOfPassengersForFloor[floor] = 0; // Previous location. */

  }
  
  /**
   * Board a passenger destined for a particular floor.
   *
   * @param floor : The floor number passenger is destined for
   */
   
  public void BoardPassenger(int floor) throws ElevatorFullException {
    if (floor <= MAXFLOOR && floor >= DEFAULT_FLOOR) {
	  if (this.GetNumberOfPassengers() >= MAXCAP) throw new ElevatorFullException();
	  numOfPassengersForFloor[floor]++; 
	  destinationRequest[floor] = true;
	} else { System.out.println("Error, no such floor exists. Or it's full and uncaught."); }
  }  
  
  /**
   * @param floors : Array of floor numbers passengers are destined for.
   */
  
  public void BoardPassenger(int[] floors) throws ElevatorFullException {
    for (int floor: floors){
	  BoardPassenger(floor);
	}
  }  
  
  /**
   * Method for registering a request from the floor with a given floor and passengers destined for it.'
   * Sets value in array for floor with request to true.
   *
   * @param floor : The originating floor of the request.
   */
  
  public void RegisterRequest(int floor) {
    this.destinationRequest[floor] = true; 
  }
  
  /**
   * Method for moving a certain amount of times.
   * Originally migrated from Main from proper HW1 solution.
   * <p>
   * Stops at floor if there's a destination request AND:
   * a) Passengers requesting to get off or b) room to take on passengers (from floor) - an extra layer of "safety".
   *
   * @param times : Number of times the elevator should move.
   */
  
  public void MoveThisMany(int times){
	for(int i=0; i < times; i++) {
	  int cFloor = this.GetCurrentFloor();
	    if (this.IsFloorRequested(cFloor) && (this.GetNumberOfPassengersForFloor(cFloor) > 0 || this.GetNumberOfPassengers() < MAXCAP)) {
		  this.stop();
		  System.out.println(this + "\n");
		}
	  this.move();
	}
  }

  /**
   * Sets up passengers waiting at floor 1, who board the elevator going to random floors.
   * At end of sequence, all passengers populated to various floors with the exception of
   * those passengers going to floor 1, who are assumed to be vacating the building. <br>
   * I assumed people generally don't use the elevator for floor 2.
   */
  
  public static void main(String a[]){
    Elevator myElevator = new Elevator();
    myElevator.SetDirectionOfTravel(UP); // Could be moved to constructor.
	System.out.println("13 Passengers arriving at floor 1.\n");
	myElevator.floors[1].SetPassengers(13);
    myElevator.RegisterRequest(1);
    myElevator.MoveThisMany(20);
	System.out.println("34 Passengers just arrived to floor 1. 14 teleporters arriving at floor 4 as well who want to take the elevator down.\n");
    myElevator.floors[1].SetPassengers(34);
	myElevator.RegisterRequest(1);
	myElevator.floors[4].SetPassengers(14);
	myElevator.RegisterRequest(4);
	myElevator.MoveThisMany(60);
	System.out.println("\n" + myElevator + " (Final Move Count " + myElevator.moveCount + ")\n"); // Final Status
	for (Floor floor : myElevator.floors) {
	  if (floor != null) System.out.println("Floor " + floor.GetId() + " population/passengers: " + floor.GetPopulation() + "/" + floor.GetPassengers());
	}
  }
}