package cscie160.hw2;

/**
 * Exception called when the elevator is full.
 */

class ElevatorFullException extends Exception {
  public ElevatorFullException(){
    super("Error: Boarding not successful, for the maximum capacity has been reached.");
	System.out.println("[Exception Thrown]: Not enough room in Elevator.");
  }
}