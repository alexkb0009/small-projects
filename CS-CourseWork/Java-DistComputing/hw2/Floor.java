package cscie160.hw2;

import java.util.Random;

/**
  * Class Floor. <br>
  * Will need to account for floors 0 and below most likely later on, esp if basement and such is implemented.<br>
  *
  * @author Alexander Balashov
  * @version 0.0.1
  * @see Elevator
  */

public class Floor {

  private int floorId; 
  private int passengers;
  private int population;
  
  /**
   * @see UnloadPassengers()
   */
  
  private static Random generator = new Random(346236);

  Floor(int id){
    this.floorId = id;
	this.passengers = 0;
  }
  
  Floor(int id, int passengers){
    this.floorId = id;
	this.passengers = passengers;
  }
  
  /**
   * Gets the floor ID aka the floor number, which is set on init.
   *
   * @return Floor Number.
   * @see Elevator
   */
  
  public int GetId(){
    return floorId;
  }

  /**
   * Methods for getting and settings passenger value.
   */
  
  public int GetPassengers(){
    return passengers;
  }
  
  public void SetPassengers(int passengers){
    this.passengers = passengers;
  }
  
  /**
   * Gets total population on this floor.
   * 
   * @return Total floor population, including passengers.
   */
  
  public int GetPopulation(){
    return this.population + this.passengers;
  }
  
  /**
   * Method for adding disembarking population from the Elevator.
   * Keeping track of how many non-passengers are on each floor. 
   * No reason other than it seemed like a simple thing to do.<br>
   * If arriving at floor one, assuming people are vacating the building.
   *
   * @param peopleGoingToThisFloor : The amount of people disembarking on floor.
   * @see #UnloadPassengers(Elevator, int)
   */
  
  public void AddToFloorPopulation(int peopleGoingToThisFloor){
   if (this.GetId() > 1) this.population += peopleGoingToThisFloor;
  }
  
  /**
   * Method for unloading the passengers which are queued on this floor.<br>
   * If there is room on elevator, and passengers waiting to get on, load them on.
   * Assuming that people on floors 3-7 (-8, -9, or higher) want to go to Floor 1.
   * People on floors 1-2 want to go up (but which floor.. hmmm.. random 3-7!.. for now).<br>
   * More "proper" to have people on floor 1 want to go up to floor 2-7, but I find people
   * often take the stairs to floor 2 & vice-versa.<br>
   * Found how to create java generators via SUNY Geneseo CS Dept site & stackoverflow.<br>
   * Clearing number of passengers for this floor in this funct to get proper "total elevator
   * passengers." 
   *
   * @param e : The Elevator object calling this method.
   * @param people : The number of passengers departing on this floor.
   */
   
  public void UnloadPassengers(Elevator e, int people){
    AddToFloorPopulation(people);
	e.ClearNumberOfPassengersForFloor(this.GetId());
	while (this.passengers > 0){
	  try {
	  	this.passengers--;
        e.BoardPassenger(this.GetId() > 2 ? 1 : generator.nextInt(e.GetMaxFloor() - 2) + 3); 
		//this.passengers--;
	  } catch (ElevatorFullException full) { 
	  	this.passengers++; // Adding passenger who was not able to get on, back to waiting queue.
		e.RegisterRequest(this.GetId());
	    break;
	  }
	}
    System.out.println("New population on floor " + this.GetId() + ": " + this.GetPopulation());
  }
  

  
}