package cscie160.hw7;

public interface ATM extends java.rmi.Remote
{
	//public void deposit(float amount) throws ATMException;
	//public void withdraw(float amount) throws ATMException;
	//public Float getBalance() throws ATMException;
	
    public void deposit(AccountInfo accountInfo, float amount) throws java.rmi.RemoteException;
	public void withdraw(AccountInfo accountInfo, float amount) throws java.rmi.RemoteException;
	public void transfer(AccountInfo accountInfoFrom, AccountInfo accountInfoTo, float amount) throws java.rmi.RemoteException;
	public Float getBalance(AccountInfo accountInfo) throws java.rmi.RemoteException;
	
	public void register(ATMListener client) throws java.rmi.RemoteException;
	public void unRegister(ATMListener client) throws java.rmi.RemoteException;
	  
	public Float getATMBalance() throws java.rmi.RemoteException;
}