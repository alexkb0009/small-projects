package cscie160.hw7;

public interface IBank extends java.rmi.Remote {
    public Account getAccount(AccountInfo accountInfo) throws java.rmi.RemoteException;
	public boolean createAccount(Float balance) throws java.rmi.RemoteException;
	public boolean createAccount(Float balance, int pin, boolean depositPerm, boolean withdrawPerm, boolean balancePerm) throws java.rmi.RemoteException;
}