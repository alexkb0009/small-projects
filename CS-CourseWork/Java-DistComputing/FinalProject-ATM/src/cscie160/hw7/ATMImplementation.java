package cscie160.hw7;

import java.util.HashMap;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.net.MalformedURLException;


public class ATMImplementation extends java.rmi.server.UnicastRemoteObject implements ATM {
  
  private Float cashBalance;
  private static String DEFAULT_HOST = "localhost";
  private Bank bankRef;
  private Security securityRef;
  private ATMFactoryImplementation factoryRef;
  
  public void setATMFactory(ATMFactoryImplementation factory) throws java.rmi.RemoteException {
    factoryRef = factory; // Good.
  }
  
  public void register(ATMListener client) throws java.rmi.RemoteException {
    factoryRef.clientList.add(client);
    System.out.println("ATM> Client registered.\n");
  }
 
  public void unRegister(ATMListener client){
    factoryRef.clientList.remove(client);
	System.out.println("ATM> Client un-registered.\n");
  }
  
  /**
   * Various methods for "setting" the ATM Implementation, to work with remote Bank and Security servers.
   * Realistically, only the first method, which takes no arguments, will be used for this.
   * The no-argument constructor looks up the Bank and Security services on the localhost machine, with no defined port.
   * Default balance set is 500.
   */
  
  public ATMImplementation() throws java.rmi.RemoteException {
    super();
    String host = ATMImplementation.DEFAULT_HOST;
	System.out.println("\nCreating ATMImplementation w/ default settings.");
	this.cashBalance = new Float(500);
	this.bind(host);
  }

  
  /** Custom cash balance **/
  public ATMImplementation(Float cashBalance) throws java.rmi.RemoteException {
    this();
	this.cashBalance = cashBalance;
  }
  
  /** Custom binding host name both Bank & Server **/
  public ATMImplementation(String bindingHostName) throws java.rmi.RemoteException {
    super();
	System.out.println("Creating ATMImplementation with bank location: //" + bindingHostName + "/bankservice");
	this.cashBalance = new Float(500);
	this.bind(bindingHostName);
  }
  
  /** Custom binding host name for Bank & Server + Custom cash balance **/
  public ATMImplementation(String bindingHostName, Float cashBalance) throws java.rmi.RemoteException { 
    this(bindingHostName);
	this.cashBalance = cashBalance;
  }
  
  
  /** Separate custom binding host names for Bank & Server **/
  public ATMImplementation(String bindingHostName, String securityHostName) throws java.rmi.RemoteException { 
    super();
 	System.out.println("Creating ATMImplementation with Bank location: //" + bindingHostName + "/bankservice & Security location: //" + securityHostName + "/securityservice");
    this.bind(bindingHostName, securityHostName);
	this.cashBalance = new Float(500);
  }

  
  /** Separate custom binding host names for Bank & Server + Custom cash balance **/
  public ATMImplementation(String bindingHostName, String securityHostName, Float cashBalance) throws java.rmi.RemoteException { 
    super();
 	System.out.println("Creating ATMImplementation with Bank location: //" + bindingHostName + "/bankservice & Security location: //" + securityHostName + "/securityservice with balance of: " + cashBalance);
    this.bind(bindingHostName, securityHostName);
	this.cashBalance = cashBalance;
  }

  
  /**
   * Method for binding private Bank & Security reference variables to instance of Bank & Security available via bindings
   * in the RMI Registry at the some host. Security check will be performed from the bank as well, to ensure security.
   * Used in testing, as both would be localhost.
   *
   * @param hostName : Host string to use for location of registry and binding. Use colon to delimit server and port number. e.g. localhost:7777.
   */
  
  private void bind(String hostName) throws java.rmi.RemoteException{
  	try {
	  System.out.println("ATM> Trying to connect to //" + hostName + "/bankservice ...");
      bankRef = (Bank)Naming.lookup("//" + hostName + "/bankservice");
	  securityRef = (Security)Naming.lookup("//" + hostName + "/securityservice");
	  System.out.println("ATM> Connected.");
	} catch (MalformedURLException mue){
	  mue.printStackTrace();
	} catch (NotBoundException nbe){
	  nbe.printStackTrace();
	} catch (UnknownHostException uhe){
	  uhe.printStackTrace();
	} catch (RemoteException re){
	  re.printStackTrace();
	}
    //bankRef = (Bank)Naming.lookup("//" + hostName + "/bankservice");
	//securityRef = (Security)Naming.lookup("//" + hostName + "/securityservice");
  }
  
  /**
   * Method for binding private Bank & Security reference variables to instance of Bank & Security available via bindings
   * in the RMI Registry at separate hosts. Security check will be performed from the bank as well, to ensure security.
   *
   * @param bindingHostName : Hostname / string for Bank service. E.g. localhost:7777.
   * @param securityHostName : Hostname string for Security service.
   */
  
  private void bind(String bindingHostName, String securityHostName){
    try {
	  bankRef = (Bank)Naming.lookup("//" + bindingHostName + "/bankservice");
	  securityRef = (Security)Naming.lookup("//" + securityHostName + "/securityservice");
	} catch (MalformedURLException mue){
	  mue.printStackTrace();
	} catch (NotBoundException nbe){
	  nbe.printStackTrace();
	} catch (UnknownHostException uhe){
	  uhe.printStackTrace();
	} catch (RemoteException re){
	  re.printStackTrace();
	}
  }
  
  
  
  /**
   * Method for getting account balance, given AccountInfo object.
   *
   * @param accountInfo : ID object with account number and pin, to determine authentication and retrieve balance.
   */
  
  public Float getBalance(AccountInfo accountInfo) throws java.rmi.RemoteException {
    if (!securityRef.authorizePIN(accountInfo)) {throw new ATMException("Account information not authorized. Check your account number & PIN code.");}
	if (!securityRef.authorizePermissions(accountInfo, Commands.BALANCE)) {throw new ATMException("Checking balance of account " + accountInfo.accountNumber + " not authorized.");}
	Float balance = bankRef.executeCommand(accountInfo, Commands.BALANCE, null);
	System.out.println("ATM> Checked balance on account: " + accountInfo.accountNumber + " - " + balance + " ");
	return balance;

  }
  
  /**
   * Method for depositing a certain amount into a given account number.
   *
   * @param accountInfo : ID object with account number and pin, to determine authentication.
   * @param amount : Float value - amount to deposit.
   */
  
  public void deposit(AccountInfo accountInfo, float amount) throws java.rmi.RemoteException, ATMException {
  	if (factoryRef != null) factoryRef.notifyClients(new TransactionNotification(accountInfo.accountNumber, Commands.DEPOSIT, amount));
    if (!securityRef.authorizePIN(accountInfo)) {throw new ATMException("Account information not authorized. Check your account number & PIN code.");}
	if (!securityRef.authorizePermissions(accountInfo, Commands.DEPOSIT)) {throw new ATMException("Depositing into account " + accountInfo.accountNumber + " not authorized.");}
    if (amount > 0){
	  Float amountOnSuccess = bankRef.executeCommand(accountInfo, Commands.DEPOSIT, amount);
	  this.cashBalance += amountOnSuccess;
	  System.out.println("ATM> Performed deposit of " + amountOnSuccess + " to account " + accountInfo.accountNumber + ". New ATM balance: " + this.getATMBalance());
	} else if (amount <= 0) {
	  throw new ATMException("Cannot deposit negative moneys.");
	} else { // Try to create new account (w/o permissions).
	  boolean created = bankRef.createAccount(amount);
      if (created) this.cashBalance += amount;
	}
  }
  
  /**
   * Method for withdrawing a certain amount from a given account number. Throws exception if not enough balance in account.
   *
   * @param accountInfo : ID object with account number and pin, to determine authentication.
   * @param amount : Float value - amount to withdraw.
   */
  
  public void withdraw(AccountInfo accountInfo, float amount) throws java.rmi.RemoteException, ATMException {
    if (factoryRef != null) factoryRef.notifyClients(new TransactionNotification(accountInfo.accountNumber, Commands.WITHDRAW, amount));
    if (!securityRef.authorizePIN(accountInfo)) {throw new ATMException("Account information not authorized. Check your account number & PIN code.");}
	if (!securityRef.authorizePermissions(accountInfo, Commands.WITHDRAW)) {throw new ATMException("Withdrawing from account " + accountInfo.accountNumber + " not authorized.");}
    if (bankRef.executeCommand(accountInfo, Commands.BALANCE, null) >= amount && amount > 0){ 
	  Float amountOnSuccess = bankRef.executeCommand(accountInfo, Commands.WITHDRAW, amount);
	  this.cashBalance -= amountOnSuccess;
	  System.out.println("ATM> Performed withdrawal of " + amountOnSuccess + " from account " + accountInfo.accountNumber + ". New ATM balance: " + this.getATMBalance());
	}
	else if (amount <= 0) {
      throw new ATMException("Can't withdraw negative money.");
	}
	else {throw new ATMException("Don't have enough money.");}
  }
  
  public void transfer(AccountInfo accountInfoFrom, AccountInfo accountInfoTo, float amount) throws java.rmi.RemoteException, ATMException {
  	if (factoryRef != null) factoryRef.notifyClients(new TransactionNotification(accountInfoFrom.accountNumber, Commands.TRANSFER, amount, accountInfoTo.accountNumber));
    if (!securityRef.authorizePIN(accountInfoFrom)) {throw new ATMException("Account information not authorized. Check your account number & PIN code.");}
	if (!securityRef.authorizePermissions(accountInfoFrom, accountInfoTo.accountNumber, Commands.TRANSFER)) {throw new ATMException("Transferring from account " + accountInfoFrom.accountNumber + " to " + accountInfoTo.accountNumber  + "not authorized.");}
	if (bankRef.executeCommand(accountInfoFrom, Commands.BALANCE, null) >= amount && amount > 0){
      bankRef.executeTransfer(accountInfoFrom, accountInfoTo.accountNumber, Commands.TRANSFER, amount);
	  System.out.println("ATM> Performed transfer of " + amount + " from account " + accountInfoFrom.accountNumber + " to " + accountInfoTo.accountNumber);
	  //if (clientRef != null) clientRef.getNotification(new TransactionNotification(accountInfoFrom.accountNumber, Commands.TRANSFER, amount, accountInfoTo.accountNumber));
	}
  }
  
  public Float getATMBalance() throws java.rmi.RemoteException {
    return this.cashBalance;
  }
  

}