package cscie160.hw7;

import java.util.HashMap;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.net.MalformedURLException;

public class BankImpl extends java.rmi.server.UnicastRemoteObject implements Bank {

	private HashMap<Integer, Account> accountMap;
	public static int accountCount = 1;
	private Security securityRef;

	/**
	 * Static method for creating static accounts for testing. Account numbers are integers, adding +1 to each new account from the previous.
	 * This allows account numbers 000001, 000002, 000003 as integers, created sequentially. Did not try to turn into Strings.
	 */

	private void createStaticAccounts(){
	  try {
        this.createAccount(new Float(0), 1234, true, true, true);
	    this.createAccount(new Float(100), 2345, true, false, true);
	    this.createAccount(new Float(500), 3456, false, true, true);
	  } catch (Exception ex){
	    ex.printStackTrace();
	  }
	}
	
	/**
	 * Method for finding security. 
	 * 
	 * @param hostname : Hostname for Security service location. Use "hostName:portNumber" syntax.
	 */
	 
	private void bindSecurity(String hostName) {
	  try {
        securityRef = (Security)Naming.lookup("//" + hostName + "/securityservice");
	  } catch (MalformedURLException mue){
	    mue.printStackTrace();
	  } catch (NotBoundException nbe){
	    nbe.printStackTrace();
	  } catch (UnknownHostException uhe){
	    uhe.printStackTrace();
	  } catch (RemoteException re){
	    re.printStackTrace();
	  }
	}
	
	/**
	 * Bank Constructor. Binds to security object, for an extra layer of security checking, and populates both self and Security Auths with initial accounts.
	 * 
	 * @param hostname : Hostname for Security service location. Use "hostName:portNumber" syntax.
	 */
	
	public BankImpl() throws java.rmi.RemoteException {
	    bindSecurity("localhost");
		this.accountMap = new HashMap<Integer, Account>(3);
		this.createStaticAccounts();
	}
	
//	/**
//	 * NO LONGER USED. Deprecated in favor of executeCommand performing it directly now.
//	 * Method for fetching an account by an AccountInfo object.
//	 * Checks Security to authorize the PIN. If allowed, returns the account object.
//	 * 
//	 * @param accountInfo : Account Info object.
//	 */
//	public Account getAccount(AccountInfo accountInfo) throws ATMException, java.rmi.RemoteException {
//	  if (this.accountMap.containsKey(accountInfo.accountNumber)) {
//	    System.out.println("BankImpl> Entering getAccount on account " + this.accountMap.get(accountInfo.accountNumber).accountNumber + " - balance:" + this.accountMap.get(accountInfo.accountNumber).getBalance());
//	    return this.accountMap.get(accountInfo.accountNumber);
//	  } else {
//	    throw new ATMException("Account doesn't exist. Deposit some money and create it!");
//	  }
//	}
	
	/**
	 * Method to execute commands to accounts through the bank, by giving an AccountInfo object with a command, and amount if necessary.
	 * If amount not needed, in the case of Commands.BALANCE, use null or any Float.
	 * <b> AccountInfo MUST have been authenticated prior to calling here. Double Security.</b>
	 *
	 * @param accountInfo : AccountInfo object which contains the Account number and PIN.
	 * @param commands : Commands enum command specifying what to perform.
	 * @param amount : Amount required for the transaction/commands, if needed, such as in deposit, withdraw, and transfer.
	 */
	
	public Float executeCommand(AccountInfo accountInfo, Commands command, Float amount) throws ATMException, java.rmi.RemoteException {
	  boolean access = securityRef.authorizePIN(accountInfo); // Double check PIN.
	  if (access) access = securityRef.authorizePermissions(accountInfo, command); // Double check Command permissions.
	  System.out.println("Bank> Re-check Security // Must be TRUE, else ATM not checking: " + access);
	  if (!access){ throw new ATMException("Access has not been granted to perform this transaction. Bank security layer. Check ATM security.");}
	  Account account;
	  if (this.accountMap.containsKey(accountInfo.accountNumber)) {
	    account = this.accountMap.get(accountInfo.accountNumber);
	  } else {
	    throw new ATMException("Account doesn't exist. Deposit some money and create it!");
	  }
	  
	  switch (command){
	    case BALANCE:
		    System.out.println("Bank> Executing Command: " + command + " on Account " + accountInfo.accountNumber + " -> " + account.getBalance());
			return account.getBalance();
			
	    case DEPOSIT:
		    System.out.println("Bank> Executing Command: " + command + " to Account " + accountInfo.accountNumber + " of " + amount);
			account.deposit(amount);
			return amount;
			
	    case WITHDRAW:
		    System.out.println("Bank> Executing Command: " + command + " from Account " + accountInfo.accountNumber + " of " + amount);
			account.withdraw(amount);
			return amount;
			
		case TRANSFER:
		    throw new ATMException("Use executeTransfer command instead.");
			
	  }
	  return new Float(0);
	}
	
	/**
	 * Method to execute transfers between bank accounts through the bank, by giving an AccountInfo object,
     * a recieving account number, with the Commands.TRANSFER command, and transfer amount.
	 * The sender needs to supply the full AccountInfo object with the PIN to transfer money, the recieving Account
	 * however only needs to supply the Account Number.
	 * <b> AccountInfo MUST have been authenticated prior to calling here. </b>
	 *
	 * @param accountInfo : AccountInfo object which contains the Account number and PIN.
	 * @param commands : Commands enum command specifying what to perform.
	 * @param amount : Amount required for the transaction/commands, if needed, such as in deposit, withdraw, and transfer.
	 */
	
	public void executeTransfer(AccountInfo accountInfoFrom, int accountNumberTo, Commands command, Float amount) throws ATMException, java.rmi.RemoteException {
	  boolean access = securityRef.authorizePIN(accountInfoFrom); // Double check PIN.
	  //if (access) access = securityRef.authorizePermissions(accountInfoFrom, accountNumberTo, command); // Double check Account Commands permissions.
	  System.out.println("Bank> Passes security double-check (PIN+Permissions): " + access);
	  if (!access){ throw new ATMException("Access has not been granted to perform this transaction. Bank security layer. Check ATM security.");}
	  
	  Account accountFrom;
	  Account accountTo;
	  
	  if (this.accountMap.containsKey(accountInfoFrom.accountNumber)) {
	    accountFrom = this.accountMap.get(accountInfoFrom.accountNumber);
	  } else {
	    throw new ATMException("Account [from] doesn't exist. Deposit some money and create it!");
	  }
	  
	  if (this.accountMap.containsKey(accountNumberTo)) {
	    accountTo = this.accountMap.get(accountNumberTo);
	  } else {
	    throw new ATMException("Account [to] doesn't exist. Check the account number.");
	  }
	  
	  if (command == Commands.TRANSFER){
	  	System.out.println("Bank> Executing Command: " + command + " from Account " + accountInfoFrom.accountNumber + " to Acccount " + accountNumberTo + " of " + amount);
	    accountFrom.transfer(accountTo, amount);
	  }
	
	}
	
	/**
	 * New account creater - sets permissions with "0" as pin and no permissions, to be 'activated', but with intro balance.
	 *
	 * @param balance : amount to credit the new account.
	 */
	
	public boolean createAccount(Float balance) throws ATMException, java.rmi.RemoteException {
	  if (this.accountMap.containsKey(BankImpl.accountCount)) {
	    BankImpl.accountCount++;
        throw new ATMException("Account number " + (BankImpl.accountCount - 1) + " already exists! Try again.");
	  } else {
	    accountMap.put(BankImpl.accountCount, new Account(BankImpl.accountCount, balance));
		System.out.println("Bank> Created account " + BankImpl.accountCount + " with balance " + accountMap.get(BankImpl.accountCount) + ". Not Activated / No PIN.");
		this.securityRef.createSecurityRecord(BankImpl.accountCount, 0, false, false, false);
		BankImpl.accountCount++;
		return true;
	  }
	}
	
	/**
	 * New account creater which takes all info necessary to 'activate' account, and registry its security.
	 *
	 * @param balance : amount to credit the new account.
	 * @param pin : PIN code of the new account, cannot start with 0 as is integer.
	 * @param depositPerm : Boolean - permission to deposit funds.
	 * @param withdrawPerm : Boolean - permission to withdraw funds.
	 * @param balancePerm : Boolean - permission to check account balance.
	 */
	 
	public boolean createAccount(Float balance, int pin, boolean depositPerm, boolean withdrawPerm, boolean balancePerm) throws ATMException, java.rmi.RemoteException {
	  if (this.accountMap.containsKey(BankImpl.accountCount)) {
	    BankImpl.accountCount++;
        throw new ATMException("Account number " +  (BankImpl.accountCount - 1) + " already exists! Try again.");
	  } else {
	    accountMap.put(BankImpl.accountCount, new Account(BankImpl.accountCount, balance));
		System.out.println("Bank> Created Account #" + BankImpl.accountCount + " with balance " + balance + ". Activating...");
		this.securityRef.createSecurityRecord(BankImpl.accountCount, pin, depositPerm, withdrawPerm, balancePerm);
		BankImpl.accountCount++;
		return true;
	  }
	}
	
}