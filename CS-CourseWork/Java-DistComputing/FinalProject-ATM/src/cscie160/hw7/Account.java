package cscie160.hw7;

public class Account extends java.rmi.server.UnicastRemoteObject implements IAccount {
  
  public int accountNumber; // To be used w/ mult accounts, in future, or something.
  
  private Float balance = new Float(0);
  //private PersonData personReference = null; Would be reference to a "Person" reference, w/First/Last name, DOB, w/e.
  
  /**
   * No argument constructor.
   * Balance is 0. Account ID is null.
   */
  public Account() throws java.rmi.RemoteException {
  
  }
  
  /**
   * Argument taking initial balance.
   *
   * @param balance : Initial balance of account. 
   */
  public Account(float balance) throws java.rmi.RemoteException {
    this();
    this.balance = balance;
  }
  
  /**
   * Argument taking initial balance & ID.
   *
   * @param balance : Initial balance of account. 
   * @param accountNumber : Unique identifier number of the account to easily identify from others in a large system. Analog to bank account #. 
   */
  public Account(int accountNumber, float balance) throws java.rmi.RemoteException {
    this(balance);
    this.accountNumber = accountNumber;
  }
  
  public Float getBalance() throws java.rmi.RemoteException {
    return this.balance;
  }
  
  public void deposit(Float amount) throws java.rmi.RemoteException {
    if (amount > 0){
	  this.balance += amount;
	}
  }
  
  public void withdraw(Float amount) throws java.rmi.RemoteException {
    if (amount > 0 && amount < balance){
	  this.balance -= amount;
	}
  }
  
  public void transfer(Account account, Float amount) throws java.rmi.RemoteException {
    if (amount > 0 && amount < balance){
	  this.balance -= amount;
	  account.deposit(amount);
	}
  }
  
  
}