package cscie160.hw7;

import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.net.MalformedURLException;

public interface Bank extends java.rmi.Remote {
	public boolean createAccount(Float balance) throws java.rmi.RemoteException;
	public boolean createAccount(Float balance, int pin, boolean depositPerm, boolean withdrawPerm, boolean balancePerm) throws java.rmi.RemoteException;
	public Float executeCommand(AccountInfo accountInfo, Commands command, Float amount) throws java.rmi.RemoteException;
	public void executeTransfer(AccountInfo accountInfoFrom, int accountNumberTo, Commands command, Float amount) throws java.rmi.RemoteException;
}

