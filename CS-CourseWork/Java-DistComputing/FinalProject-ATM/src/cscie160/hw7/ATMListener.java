package cscie160.hw7;


public interface ATMListener extends java.rmi.Remote {

  public void getNotification(TransactionNotification notification) throws java.rmi.RemoteException;

}