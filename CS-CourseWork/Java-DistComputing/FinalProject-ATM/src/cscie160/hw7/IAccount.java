package cscie160.hw7;

public interface IAccount extends java.rmi.Remote {
  public Float getBalance() throws java.rmi.RemoteException;
  public void deposit(Float amount) throws java.rmi.RemoteException;
  public void withdraw(Float amount) throws java.rmi.RemoteException;
  public void transfer(Account account, Float amount) throws java.rmi.RemoteException;
}