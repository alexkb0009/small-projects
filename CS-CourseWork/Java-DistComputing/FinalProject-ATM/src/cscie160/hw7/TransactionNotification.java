package cscie160.hw7;

public class TransactionNotification implements java.io.Serializable {
  public int accountNumber;
  public int accountNumber2;
  public Float amount;
  public Commands command;
  
  public String toString(){
    String str = "Account " + accountNumber + " attempting to " + command;
	if (accountNumber2 > 0 && command == Commands.TRANSFER) str += " $" + amount + " to Account " + accountNumber2;
	else str += " $" + amount;
	return str;
  }
  
  public TransactionNotification(int accountNumber, Commands command, Float amount){
    this.accountNumber = accountNumber;
	this.command = command;
	this.amount = amount;
  }
  
  public TransactionNotification(int accountNumber, Commands command, Float amount, int accountNumber2){
    this(accountNumber, command, amount);
	this.accountNumber2 = accountNumber2;
  }
  
}