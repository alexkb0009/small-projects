package cscie160.hw7;

public class ATMImplementationTest {
  
  private ATMImplementation atmImpl;
  
  public void createTest(){
    try {
    System.out.println("Testing creation of ATM Implementation.");
    atmImpl = new ATMImplementation();
	System.out.println("Created ATM Implementation without error, checking balances, should be filled with static vals.");
	try {System.out.println("Should Exist - 0000001: " + atmImpl.getBalance(0000001)); } catch (ATMException atme) {atme.printStackTrace();}
	try {System.out.println("Should Exist - 0000002: " + atmImpl.getBalance(0000002)); } catch (ATMException atme) {atme.printStackTrace();}
	try {System.out.println("Should Exist - 0000003: " + atmImpl.getBalance(0000003)); } catch (ATMException atme) {atme.printStackTrace();}
    try {System.out.println("Doesn't Exist - 0000004: " + atmImpl.getBalance(0000004)); } catch (ATMException atme) {atme.printStackTrace();}
	
	System.out.println("\n\nOK");
	} catch (Exception ex) {
	  ex.printStackTrace();
	}
  }
  
  public void depositTest(){
  try {
    System.out.println("Init Balances of 1, 2, 3, and [non-existant] 4:\n" + atmImpl.getBalance(0000001) + "\n" + atmImpl.getBalance(0000002) + "\n" + atmImpl.getBalance(0000003) + "\n" + atmImpl.getBalance(0000004));
    try {
	  System.out.println("Testing Deposit of 1000 into account 0000001..");
	  atmImpl.deposit(0000001, 1000);
	  System.out.println("0000001 Balance: " + atmImpl.getBalance(0000001));
	} catch (ATMException atme) {
      atme.printStackTrace();
	  System.out.println("Catch when not enough balance on withdraw. And adjust response to client.\n");
    } 
	
	try {
	  System.out.println("Testing Deposit of 1123 into account 0000002..");
	  atmImpl.deposit(0000002, 1123);
      System.out.println("0000002 Balance: " + atmImpl.getBalance(0000002));
	} catch (ATMException atme) {
      atme.printStackTrace();
	  System.out.println("Catch when not enough balance on withdraw. And adjust response to client.\n");
    } 
	
	try {
	  System.out.println("Testing Deposit of -100 into account 0000001..");
	  atmImpl.deposit(0000001, -100);
	  System.out.println("0000001 Balance: " + atmImpl.getBalance(0000001));
	} catch (ATMException atme) {
      atme.printStackTrace();
	  System.out.println("Catch when not enough balance on withdraw. And adjust response to client.\n");
    } 
	
	try {
	  System.out.println("Testing Deposit of -600 into account 0000003..");
	  atmImpl.deposit(0000003, -600);
	  System.out.println("0000003 Balance: " + atmImpl.getBalance(0000003));
	} catch (ATMException atme) {
      atme.printStackTrace();
	  System.out.println("Catch when not enough balance on withdraw. And adjust response to client.\n");
    } 
	
	try {
	  System.out.println("Testing Deposit of 1000 into account 0000004 (non-existant)..");
	  atmImpl.deposit(0000004, 1000);
	  System.out.println("0000004 Balance: " + atmImpl.getBalance(0000004));
	} catch (ATMException atme) {
      atme.printStackTrace();
	  System.out.println("Catch when not enough balance on withdraw. And adjust response to client.\n");
    } 
	
	System.out.println("\n\nOK");
  } catch (Exception ex) {
    ex.printStackTrace();
  }
  }
  
  public void withdrawTest(){
  try {
    try {
      System.out.println("Init Balances of 1, 2, 3, and [non-existant] 4:");
	  System.out.println(atmImpl.getBalance(0000001));
	  System.out.println(atmImpl.getBalance(0000002));
	  System.out.println(atmImpl.getBalance(0000003));
	  System.out.println(atmImpl.getBalance(0000004));
	} catch (ATMException atme) {
      atme.printStackTrace();
    } 
	
	try {
	  System.out.println("Testing withdrawal of 23 from account 0000002..");
	  atmImpl.withdraw(0000002, 23);
      System.out.println("0000002 Balance: " + atmImpl.getBalance(0000002));
	} catch (ATMException atme) {
      atme.printStackTrace();
    } 
	
	try {
	  System.out.println("Testing withdrawal of -100 from account 0000001..");
	  atmImpl.withdraw(0000001, -100);
	  System.out.println("0000001 Balance: " + atmImpl.getBalance(0000001));
	} catch (ATMException atme) {
      atme.printStackTrace();
    } 
	
	try {
	  System.out.println("Testing withdrawal of 1000 from account 0000004 (non-existant)..");
	  atmImpl.withdraw(0000004, 1000);
	  System.out.println("0000004 Balance: " + atmImpl.getBalance(0000004));
	} catch (ATMException atme) {
      atme.printStackTrace();
    } 
	
	try {
	  System.out.println("Testing withdrawal of 1000 from account 0000001..");
	  atmImpl.withdraw(0000001, 1000);
	  System.out.println("0000001 Balance: " + atmImpl.getBalance(0000001));
    } catch (ATMException atme) {
      atme.printStackTrace();
    } 
	
	System.out.println("\n\nOK");
  } catch (Exception ex) {
    ex.printStackTrace();
  }
  }
  
  public static void main(String argv[]) {
    try {
      ATMImplementationTest myTest = new ATMImplementationTest();
	  myTest.atmImpl = new ATMImplementation();
	  if (argv.length < 1){ System.out.println("Usage: ATMImplementationTest + one of: 1 - create, 2 - deposit, 3 - withdraw."); return; }
	
    switch (Integer.parseInt(argv[0])) {
	  case 1:
	    myTest.createTest();
		break;
	  case 2:
	    myTest.depositTest();
		break;
	  case 3:
	    myTest.withdrawTest();
		break;
	  default: System.out.println("Error, wrong test specified."); break;
	}
	} catch (Exception ex) {
	  System.out.println("Something, somewhere, went wrong.");
	}
	System.exit(1);
  }
}
