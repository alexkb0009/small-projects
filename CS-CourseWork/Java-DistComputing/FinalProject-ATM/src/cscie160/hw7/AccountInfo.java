package cscie160.hw7;

public class AccountInfo implements java.io.Serializable {
  public final int accountNumber;
  private int pinNumber;
  
  /** 
   * Proper constructor to use. Sets account and pin numbers.
   *
   * @param accountNumber The account number to set. 
   * @param pinNumber The PIN to set. 
   */
  
  public AccountInfo (int accountNumber, int pinNumber){
    this.accountNumber = accountNumber;
	this.pinNumber = pinNumber;
  }
  
  public int getPIN(){
    return this.pinNumber;
  }
  
}