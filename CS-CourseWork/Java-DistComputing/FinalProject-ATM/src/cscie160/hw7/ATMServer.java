package cscie160.hw7;

import java.rmi.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;

public class ATMServer {
  //private static int DEFAULT_PORT = 7777;
  private static String DEFAULT_HOST = "localhost";
  
  public static void main(String[] args) throws java.rmi.RemoteException {
    int port = 0; //int port = Server.DEFAULT_PORT;
    String host = ATMServer.DEFAULT_HOST;
	try {
      if (args.length >= 2) port = Integer.parseInt(args[1]);
	  if (args.length >= 1) host = args[0];
	} catch (Exception ex){
	  System.out.println("Oops, couldn't set something, probably the port.");
	  return;
	}
	
	String name = port != 0 ? "//" + host + ":" + port + "/atmfactory" : "//" + host + "/atmfactory" ;
	ATMFactoryImplementation factoryInstance = new ATMFactoryImplementation();
	try {
      System.out.println("Registering Factory Object @ " + name);
	  Naming.rebind(name, factoryInstance);
	  System.out.println("Succesfully Registered Factory Object on " + name + "; waiting for client(s)...");
	} catch (Exception ex) { 
	  System.out.println("Server Main Error --> No Registry Found? ");
	  ex.printStackTrace(); 
	}
	//System.out.println("Server main exiting... Unless there was an exception thrown the server (aka FactoryImplementation binding) is still running.");
  }
  
}