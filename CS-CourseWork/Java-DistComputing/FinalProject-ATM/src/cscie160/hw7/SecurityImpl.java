package cscie160.hw7;

import java.util.HashMap;

public class SecurityImpl extends java.rmi.server.UnicastRemoteObject implements Security {

    private HashMap<Integer, Integer> accountInfo;
	private HashMap<Integer, boolean[]> accountPermissions; 
	
    public SecurityImpl() throws java.rmi.RemoteException {
		accountInfo = new HashMap<Integer, Integer>(3);
		accountPermissions = new HashMap<Integer, boolean[]>(3);
		//createDefaultAccountsAndPins();
	}
	
	/** Currently not used, creates security records. Bank initiates this now. 
	
	private void createDefaultAccountsAndPins () throws java.rmi.RemoteException {
		createSecurityRecord(0000001, 1234, true, true, true);
		createSecurityRecord(0000002, 2345, true, false, true);
		createSecurityRecord(0000003, 3456, false, true, true);
	}
	
	**/
	
	public void createSecurityRecord(int accountNumber, int pin, boolean depositPerm, boolean withdrawPerm, boolean balancePerm) throws java.rmi.RemoteException {
	   try {
	     accountInfo.put(accountNumber, pin);
		 accountPermissions.put(accountNumber, new boolean[]{depositPerm, withdrawPerm, balancePerm});
		 System.out.println("Security> Created security record for Account " + accountNumber + " -\nDeposit: " + depositPerm + "  Withdraw: " + withdrawPerm + "  Balance: " + balancePerm + "\n");
	   } catch (Exception error) {
		 System.out.println("Security> Some sort of error: \n");
		 error.printStackTrace();
	   }
	}
	
	public boolean authorizePIN(AccountInfo accountInfoToCheck) throws ATMException, java.rmi.RemoteException {
	  int pin = 0;
	  if (this.accountInfo.containsKey(accountInfoToCheck.accountNumber)) {
	    pin = this.accountInfo.get(accountInfoToCheck.accountNumber);
	  }
	  if (pin != 0 && accountInfoToCheck.getPIN() == pin){
	    System.out.println("Security> Account " + accountInfoToCheck.accountNumber + "'s PIN authorized.");
	    return true;
	  } else if (accountInfoToCheck.getPIN() == 0) {
	    System.out.println("Security> Error: PIN has not been set for this account. Contact a bank employee to set one today.");
	    throw new ATMException("Security> PIN has not been set for this account. Contact a bank employee to set one today.");
	  } else {
	  	System.out.println("Security> Account " + accountInfoToCheck.accountNumber + "'s PIN is incorrect.");
	    return false;
	  }
	}
	
	public boolean authorizePermissions(AccountInfo accountInfoToCheck, Commands command) throws java.rmi.RemoteException {
	  boolean[] permissions;
	  if (this.accountPermissions.containsKey(accountInfoToCheck.accountNumber)) {
	    permissions = this.accountPermissions.get(accountInfoToCheck.accountNumber);
	  } else {
	    System.out.println("Account permissions not found.");
	    return false;
	  }
	  
	  switch (command){
	    case BALANCE:
		    System.out.println("Security> Account " + accountInfoToCheck.accountNumber + " has auth for " + command + ": " + permissions[2]);
			return permissions[2];
			
	    case DEPOSIT:
		    System.out.println("Security> Account " + accountInfoToCheck.accountNumber + " has auth for " + command + ": " + permissions[0]);
			return permissions[0];
			
	    case WITHDRAW:
		    System.out.println("Security> Account " + accountInfoToCheck.accountNumber + " has auth for " + command + ": " + permissions[1]);
			return permissions[1];
			
	    case TRANSFER:
		    System.out.println("Security> Must include two accounts to authorize a transfer.");
			return false;
	  }
	  return false;
	}
	
	public boolean authorizePermissions(AccountInfo accountInfoToCheck, int accountNumberTo, Commands command) throws java.rmi.RemoteException {
	  if (command != Commands.TRANSFER) {
	    System.out.println("Security> Must be a transfer auth request. Use authorizePermission(AccountInfo accountInfo, Commands command) for other permission auth requests.");
		return false;
	  }
	
	  boolean[] permissions1;
	  if (this.accountPermissions.containsKey(accountInfoToCheck.accountNumber)) {
	    permissions1 = this.accountPermissions.get(accountInfoToCheck.accountNumber);
	  } else {
	    System.out.println("Security> [From] Account permissions not found.");
        return false;
	  }
	  
	  boolean[] permissions2;
	  if (this.accountPermissions.containsKey(accountNumberTo)) {
	    permissions2 = this.accountPermissions.get(accountNumberTo);
	  } else {
	    System.out.println("Security> [To] Account permissions not found.");
	    return false;
	  }
      
	  if (permissions1 == null || permissions2 == null) {
	    System.out.println("Security> Must include two accounts to authorize a transfer.");
		return false;
	  }
	  
	  if (permissions1[1] && permissions2[0]){
	  	System.out.println("Security> Accounts " + accountInfoToCheck.accountNumber + " and " + accountNumberTo + " have authorization for " + command + ": true");
	    return true;
	  } else {return false;}
	  
	}

}