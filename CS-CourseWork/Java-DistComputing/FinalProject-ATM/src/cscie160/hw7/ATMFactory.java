package cscie160.hw7;

public interface ATMFactory extends java.rmi.Remote
{
	public ATM getATM() throws java.rmi.RemoteException;
}