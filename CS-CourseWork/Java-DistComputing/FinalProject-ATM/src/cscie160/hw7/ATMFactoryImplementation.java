package cscie160.hw7;

import java.util.*;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;   

public class ATMFactoryImplementation extends java.rmi.server.UnicastRemoteObject implements ATMFactory { 

  public LinkedList <ATMListener> clientList = new LinkedList <ATMListener>();

  public ATMFactoryImplementation() throws java.rmi.RemoteException {
    super();
  }
  
  /**
   * Returns a new ATM Implementation, which creates new accounts w/predetermined balances.
   * 
   * Perhaps this class should have a [static?] reference to ONE ATMImplementation, and return reference to that with this method, rather than creating a new one each time, with new Accounts, with the same account number...
   * Bcuz in real world, that's kind of more how'd I'd expect it to function. 
   */
  
  public ATM getATM() throws java.rmi.RemoteException {
    ATMImplementation atmImpl = new ATMImplementation();
	atmImpl.setATMFactory(this);
    return atmImpl;
  };

  public void notifyClients(TransactionNotification notification) throws java.rmi.RemoteException {
    Iterator iterator = clientList.iterator();
	while (iterator.hasNext()){
	  System.out.println("Factory> Sending Transaction Notification [" + clientList.size() + " clients] - " + notification.command + " on Account " + notification.accountNumber);
	  ((ATMListener)iterator.next()).getNotification(notification);
	}
  }
  
  
  
}