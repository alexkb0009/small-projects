package cscie160.hw7;

import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.net.MalformedURLException;

public interface Security extends java.rmi.Remote {

  	public void createSecurityRecord(int accountNumber, int pin, boolean depositPerm, boolean withdrawPerm, boolean balancePerm) throws java.rmi.RemoteException;
	public boolean authorizePIN(AccountInfo accountInfoToCheck) throws java.rmi.RemoteException;
	public boolean authorizePermissions(AccountInfo accountInfoToCheck, Commands command) throws java.rmi.RemoteException;
	public boolean authorizePermissions(AccountInfo accountInfoToCheck, int accountNumberTo, Commands command) throws java.rmi.RemoteException;

}