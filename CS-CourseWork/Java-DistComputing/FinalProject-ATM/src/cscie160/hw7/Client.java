package cscie160.hw7;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;

public class Client implements ATMListener {

  public Client() throws java.rmi.RemoteException{

  }

  public static void main(String[] args) throws java.rmi.RemoteException {
    ATM atm = null;
	String host = "localhost";
	
	try {
	  if (args.length > 0) host = args[0];
	  if (args.length > 1) host += ":" + Integer.parseInt(args[1]) /* To make sure is int */ ;
	} catch (Exception e) {
	  e.printStackTrace();
	}
	
	System.out.println("Client> Starting Client, looking for //" + host + "/atmfactory ...");
	
    try {
	  ATMFactory factory = (ATMFactory)Naming.lookup("//" + host + "/atmfactory");
	  atm = factory.getATM();
	  Client client = new Client();
	  System.out.println("Client> Registering Client stub w/ ATMFactory.\n");
	  ATMListener stub = (ATMListener)UnicastRemoteObject.exportObject(client, 0);
	  
	  atm.register(stub); // Register Client/ATMListener stub, to recieve notifications.
	  testATM(atm); // Do ATM tests.
	  atm.unRegister(client); // Unregister when done. Having all here allows to start up new Clients after and access still-running Servers.
	
	} catch (MalformedURLException mue){
	  mue.printStackTrace();
	} catch (NotBoundException nbe){
	  nbe.printStackTrace();
	} catch (UnknownHostException uhe){
	  uhe.printStackTrace();
	} catch (RemoteException re){
	  re.printStackTrace();
	}
	
	
	System.out.println("\nClient> main() complete. Exiting..");
	System.exit(0);
  }
  
  public void getNotification(TransactionNotification notification) throws java.rmi.RemoteException {
    System.out.println("<TransactionNotification>\n" + notification.toString() + "\n");
  }
  
  public static AccountInfo getAccountInfo(int accountNumber, int pinNumber) {
    AccountInfo infoObj = new AccountInfo(accountNumber, pinNumber);
	// DEBUGGING: System.out.println("AccontInfo obj created: " + infoObj.accountNumber + " : " + infoObj.getPIN());
    return infoObj;
  }
    
  public static void testATM(ATM atm) {
      if (atm!=null) {
         printBalances(atm);
         performTestOne(atm);
         performTestTwo(atm);
         performTestThree(atm);
         performTestFour(atm);
         performTestFive(atm);
         performTestSix(atm);
         performTestSeven(atm);
         performTestEight(atm);
         performTestNine(atm);
         printBalances(atm);
      }
   }        
   public static void printBalances(ATM atm) {        
      try {
         System.out.println("Balance(0000001): "+atm.getBalance(getAccountInfo(0000001, 1234)));
         System.out.println("Balance(0000002): "+atm.getBalance(getAccountInfo(0000002, 2345)));
         System.out.println("Balance(0000003): "+atm.getBalance(getAccountInfo(0000003, 3456)));
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
   public static void performTestOne(ATM atm) {       
      try {
         atm.getBalance(getAccountInfo(0000001, 5555));
      } catch (Exception e) {
         System.out.println("Failed as expected: "+e);
      }
   }
   public static void performTestTwo(ATM atm) {       
      try {
         atm.withdraw(getAccountInfo(0000002, 2345), 500);
      } catch (Exception e) {
         System.out.println("Failed as expected: "+e);
      }
   }
   public static void performTestThree(ATM atm) {        
      try {
         atm.withdraw(getAccountInfo(0000001, 1234), 50);
      } catch (Exception e) {
         System.out.println("Failed as expected: "+e);
      }
   }
   public static void performTestFour(ATM atm) {         
      try {
         atm.deposit(getAccountInfo(0000001, 1234), 500);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
   public static void performTestFive(ATM atm) {         
      try {
         atm.deposit(getAccountInfo(0000002, 2345), 100);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
   public static void performTestSix(ATM atm) {       
      try {
         atm.withdraw(getAccountInfo(0000001, 1234), 100);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
   public static void performTestSeven(ATM atm) {        
      try {
         atm.withdraw(getAccountInfo(0000003, 3456), 300);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
   public static void performTestEight(ATM atm) {        
      try {
         atm.withdraw(getAccountInfo(0000001, 1234), 200);
      } catch (Exception e) {
         System.out.println("Failed as expected: "+e);
      }
   }
   public static void performTestNine(ATM atm) {        
      try {
         atm.transfer(getAccountInfo(0000001, 1234),getAccountInfo(0000002, 2345), 100);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
  
}