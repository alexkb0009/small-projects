package cscie160.hw7;

import java.rmi.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;

public class BankServer {
  //private static int DEFAULT_PORT = 7777;
  private static String DEFAULT_HOST = "localhost";
  
  
  public static void main(String[] args) throws java.rmi.RemoteException {
    int port = 0; //int port = Server.DEFAULT_PORT;
    String bankHost = BankServer.DEFAULT_HOST;
	String securityHost = BankServer.DEFAULT_HOST;
	try {
      //if (args.length >= 2) port = Integer.parseInt(args[1]);
	  if (args.length >= 1) bankHost = args[0];
	  if (args.length >= 2) securityHost = args[1];
	  if (args.length >= 3) port = Integer.parseInt(args[2]);
	} catch (Exception ex){
	  System.out.println("Oops, couldn't set something. Usage: cscie160.hw7.BankServer [+ Bank Host] [+ Security Host] [+ Port]\nDefaults: localhost localhost [ not specified ]");
	  return;
	}
	
	String bankName = port != 0 ? "//" + bankHost + ":" + port + "/bankservice" : "//" + bankHost + "/bankservice" ;
	String securityName = port != 0 ? "//" + securityHost + ":" + port + "/securityservice" : "//" + securityHost + "/securityservice" ;

	try {
      System.out.println("BankServer> Registering Bank + Security objects @ " + bankName + " & " + securityName);
	  SecurityImpl securityInstance = new SecurityImpl();
	  Naming.rebind(securityName, securityInstance);
	  BankImpl bankInstance = new BankImpl();
	  Naming.rebind(bankName, bankInstance);
	  System.out.println("BankServer> Succesfully registered "+ securityName +" & "+ bankName +", waiting for client(s)...\n");
	} catch (Exception ex) { 
	  System.out.println("Server Main Error --> No Registry Found? \n\n");
	  ex.printStackTrace(); 
	}
  }

}