# README #

This repository contains various small projects created while at ICS (public) and on own personal or academic time but which aren't large enough to warrant their own repository.
Some of this code is several years old.

### Demo ###

Most of this code can be demo'd at http://akb.productions/sp/, which is aligned to this repository.

For example: 

 - http://akb.productions/sp/CS-CourseWork/EconCS-ProjectTeamSelector/
 - http://akb.productions/sp/Newsletter-Manager/
 - http://akb.productions/sp/HTML5-Geolocation/
 - http://akb.productions/sp/WebGL-Tiles/
 - etc.