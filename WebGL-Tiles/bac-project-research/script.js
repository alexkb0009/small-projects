/**
  * Script for use w/ ThreeJS and WebGL as part of visualization research for BAC Project Research Seminar Course
  * (C) 2013 Alexander Balashov
  */ 
  
/*** Variable definition & initialization ***/
/* Three.JS Vars */
var VIEW_ANGLE = 55, NEAR = 0.1, FAR = 1000; 		// Three.JS scene-specific constant variables.
var aspect, scene, camera, renderer; 				// Three.JS scene-specific variables.

/* Custom Variables */
var viewportContainer, menuBox;
var lights, materials, geometry;					// Default scene elements. Define these in functions below.
var groupA, groupB, ratioInput, ratioTo1, multiplicationFactor;

/**
 * Initialization function, to be called on page load.
 */
function initializeScene(){
  viewportContainer = document.getElementById("viewport-container");
  menuBox = document.getElementById("menu-box");
  onWindowResize();
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(VIEW_ANGLE, aspect, NEAR, FAR);
  camera.position.z = 20;
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(viewportContainer.clientWidth * 1.25, viewportContainer.clientHeight * 1.25);
  
  document.getElementById("viewport-container").appendChild(renderer.domElement);
  
  initializeBindings();
  createBaseSceneElements();
  renderSceneContinuously();
}

/**
 * Function called on window resizing.
 */
function onWindowResize(){
  console.log("Window resized. h: " + window.innerHeight + " w: " + window.innerWidth);
  viewportContainer.style.height = window.innerHeight - menuBox.clientHeight;
  aspect = viewportContainer.clientWidth / viewportContainer.clientHeight;
  if (renderer != null) {
    renderer.setSize(viewportContainer.clientWidth * 1.25, viewportContainer.clientHeight * 1.25);
  }
}

/**
 * Define and initialize some lights & materials.
 */
 
function createLights(){
  if (lights == null) lights = new Object();
  lights.pointLightA = new THREE.PointLight(0xFFFFFF, .8);
  lights.pointLightA.position.set(30, -50, 70);
  lights.pointLightB = new THREE.PointLight(0xFFFFFF, .4);
  lights.pointLightB.position.set(40, -80, 50);
  
  scene.add(lights.pointLightA); // Add them in.
  scene.add(lights.pointLightB);
}

function defineMaterials(){
  if (materials == null) materials = new Object();
  materials.grayGloss = new THREE.MeshLambertMaterial({color: 0x999999, specular: 0xa9cfff, ambient: 0xffffff, wireframe: false});
  materials.whiteGloss = new THREE.MeshLambertMaterial({color: 0xffffff, specular: 0xa9cfff, ambient: 0xffffff, wireframe: false});  
  materials.orangeHighlight = new THREE.MeshPhongMaterial({color: 0xFF9900, ambient: 0xff3300, combine: THREE.MixOperation, reflectivity: 0.3});
}

/**
 * Define geometry.
 */
 
function createGeometry(){
  geometry = new THREE.CubeGeometry(1,1,1);
}

/*****
 ***** Visualization Logic
 *****/

var columns;

/**
 * Entrance function into ratio visualization. Calculates 1 - to - X ratio.
 * Input:
 *   ratio : decimal number 0 - 1 indicating probability ratio. 
 */

function runRatio(ratio){
  ratioTo1 = 1 / ratio;
  createVisualizationElements(Math.floor(ratioTo1));
  return ratioTo1;
}

function setColumns(elementsNumber){
  columns = Math.floor(Math.sqrt(elementsNumber) + 0.5);
  return columns;  
}

function createVisualizationElements(number){
  sceneCleanUp();
  groupA = new THREE.Object3D();
  groupB = new THREE.Object3D();
  setColumns(number * multiplicationFactor);
  
  for (var i = 0; i < multiplicationFactor; i++) {
    groupA.children[i] = new THREE.Mesh(geometry, materials.orangeHighlight);
    //groupA.children[i].position.x = -3 * (i + 1) + 2;
	groupA.children[i].position.x =  -(i % columns) * 3;
    groupA.children[i].position.y = -(i - (i % columns)) * 3 / columns;
  }
  
  for (var i = 0; i < (number - 1) * multiplicationFactor; i++){ // Number - 1, as groupA has one (e.g. for 1 out of a total 4 = .25)
    groupB.children[i] = new THREE.Mesh(geometry, materials.whiteGloss);
    groupB.children[i].position.x = ((i % columns)) * 3 + 3;
    groupB.children[i].position.y = -(i - (i % columns)) * 3 / columns;
    groupB.children[i].position.z = 0;
  }
  scene.add(groupA);
  scene.add(groupB);
  if (camera != null){
    camera.position.y = - Math.sqrt(number * multiplicationFactor) * 1.33;
  }
  
}

var tempObj;
function sceneCleanUp(){
  console.log("Cleaning up scene..");
  if (groupA == null || groupB == null) return;
  scene.remove(groupA);
  scene.remove(groupB);
  groupA = null;
  groupB = null;
}

function renderSceneContinuously(){
  requestAnimationFrame(renderSceneContinuously);
  renderer.render(scene, camera);
}

/***
 *** Mouse wheel position = camera z.
 ***/
 
var wheelDelta = 0;

function wheelAction(delta){
  if (camera.position.z - delta < 0) return;
  camera.position.z -= parseInt(delta);
  console.log(camera.position.z);
}
			
function adjustMouseWheeel(event){
  if (!event) event = window.event;
  if (event.wheelDelta) delta = parseInt(event.wheelDelta/120);
  else if (event.detail) delta = parseInt(-event.detail/3);
  if (delta) {
    wheelAction(delta);
  }
  if (event.preventDefault) event.preventDefault();
  event.returnValue = false;
}

/***
 *** UI Button Functions
 ***/
 
 /**
 * Function(s) initializing the visualization process on button press.
 * Checks input for validity before sending for processing.
 */
 
 function visualizeButtonClick(){
   ratioInput = document.getElementById("ratio-input").value;
   multiplicationFactor = document.getElementById("factor-input").value;
   ratioInput = parseFloat(ratioInput);
   multiplicationFactor = parseInt(multiplicationFactor);
   
   if (ratioInput == null) {
     alert("Error: You must input a number.");
	 return;
   }
   if (ratioInput > 1) { 
     alert("Maximum value is 1.");
	 return;
   }
   if (ratioInput <= 0) { 
     alert("Minimum value is 0.0001"); 
	 return;
   }
   
   if (multiplicationFactor == null) {
     alert("Error: You must input a multiplication factor.");
	 return;
   }
   if (multiplicationFactor < 1) {
     alert("Error: Minimum factor is 1.");
	 return;
   }
   
   console.log("Input value: " + ratioInput);
   document.getElementById("ratio-input").style.borderColor = "#555";		// Give UI feedback.
   document.getElementById("ratio-input").style.color = "#fff";
   runRatio(ratioInput); 													// Run process if no errors.
 }
 
 function clearInputIfInitial(e){
   this.style.color = "#ccc";
   this.style.borderColor = "#ccc";
   if (this.getAttribute("state") == null){
     this.setAttribute("state", "modified");
	 this.value = "";
   }
 }
 
 function navKeyPress(e){
   if (camera == null) return;
   //alert(e.keyCode);
   if (e.keyCode == 40) camera.position.z++; // Up
   if (e.keyCode == 38) camera.position.z--; // Down
   if (e.keyCode == 37) { // Left
     camera.rotation.y += .1;
	 groupB.rotation.y += .1;
   }
   if (e.keyCode == 39) { // Right
     camera.rotation.y -= .1;
	 groupB.rotation.y -= .1;
   }
   
 }

/**
 * Function(s) initializing the bindings.
 */

function initializeBindings(){
  /** Mouse Wheel **/
  if (window.addEventListener){
    window.addEventListener('DOMMouseScroll', adjustMouseWheeel, false);
  }
  window.onmousewheel = document.onmousewheel= adjustMouseWheeel;
  
  /** UI/Button Controls **/
  document.getElementById("visualize-button").onclick = visualizeButtonClick;
  document.getElementById("ratio-input").onclick = clearInputIfInitial;
  document.getElementById("factor-input").onclick = clearInputIfInitial;
  window.onkeydown = navKeyPress;
  
}

function createBaseSceneElements(){
  defineMaterials();
  createLights();
  createGeometry();
}

/*****
 ***** Final bindings & initialization function call on page load.
 *****/
 
 window.onload = function(){
   initializeScene();
   window.onresize = onWindowResize;
   
 }