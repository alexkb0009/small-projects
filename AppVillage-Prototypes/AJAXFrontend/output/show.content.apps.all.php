<?php

/** 
 *  Shows templated apps (all, not by category, etc.) from sc-backend.
 *
 *  Parameters:
 *   - template: (optional) template file to use. 
 *   - range: (optional) # of apps to return.
 *   - headerType: Type of header and/or other styling to set, above returned apps.
 * 
 */

include('../data_source.sc_backend_xmlrpc.inc');
include('../utilities.inc');

// Default Parameter Values
$range = 50; 
$template = "grid";
$headerType = "appspage";
 
if (isset($_GET['range'])) {
  $range = (int) $_GET['range'];
};

if (isset($_GET['template'])) {
  $template = (string) $_GET['template'];
};

if (isset($_GET['headerType'])) {
  $headerType = (string) $_GET['headerType'];
};


// Make Header
if ($headerType == 'frontpage') { // ToDo: make template file thingy for dif types of headers if needed.

  print '<div id="apps-header" class="' . $template . ' ' . $_GET['headerType'] . '">';
  
  if (isset($_GET['headerContent'])){ 
    print $_GET['headerContent']; 
  } else {
    print 'Welcome to <b>' . $settings->frontend['site_name'] . '</b>';
  }
  
  print '</div>'; 

 };

$apps = xmlrpc_call('sc.getProductNames', array());
$i = count($apps);
$e = max($i - $range, 0); 

print '<div id="apps-container" class="' . $template . ' ' . $headerType .'">';

// Print out apps.
while($i > $e){
  $i--; 
  $app = $apps[$i]; /* "$app" is required by template structure */
  $appsDir = '/' . $settings->upload_dirs['root_dir'] . '/' . $settings->upload_dirs['icons'] . '/'  . $app['name_id'];
  $app['icon_url'] = $settings->frontend['def_app_icon'];
  $app['icon_size'] = [120, 120];
  if (file_exists(ROOT_DIR . $appsDir)){
    $files = getDirFiles(ROOT_DIR . $appsDir);
    if (count($files) > 0) {
      $app['icon_url'] = $files[0];
      $app['icon_size'] = getimagesize($app['icon_url']);
      if (count($files) > 1){
        $j = 1;
        while ($j < count($files)){  
          if (getimagesize($files[$j])[0] > $app['icon_size'][0]){
            $app['icon_url'] = $files[$j];
            $app['icon_size'] = getimagesize($files[$j]);
          }
          $j++;
        }
      }
      $app['icon_url'] = $appsDir . '/' . basename($app['icon_url']); // Replace file path with proper URL.
    }
  }

  include('../template/app.' . $template . '.template.php');
}

print '</div>';

?>
