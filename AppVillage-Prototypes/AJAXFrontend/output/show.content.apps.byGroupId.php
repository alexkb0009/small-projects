<?php

/** 
 *  Shows templated apps by category from sc-backend.
 *
 *  Parameters:
 *   - template: (optional) template file to use. 
 *   - range: (optional) # of apps to return.
 *   - headerType: Type of header and/or other styling to set, above returned apps.
 * 
 */

include('../data_source.sc_backend_xmlrpc.inc');
include('../utilities.inc');

// Default Parameter Values
$range = 50; 
$template = "grid";
$headerType = "appspage";
$group_id = NULL; // REQUIRED
 
if (isset($_GET['range'])) {
  $range = (int) $_GET['range'];
};

if (isset($_GET['template'])) {
  $template = (string) $_GET['template'];
};

if (isset($_GET['headerType'])) {
  $headerType = (string) $_GET['headerType'];
};

if (isset($_GET['group_id'])) {
  $group_id = (int) $_GET['group_id'];
};

$groupInfo = xmlrpc_call('sc.getGroupNameByGroupID', array($group_id));
$groupApps = xmlrpc_call('sc.getNamesByGroupID', array('group_id' => $group_id));
$childGroups = [];
$groupApps = $groupApps['data'];

// Make Header
if ($headerType == 'frontpage') { // ToDo: make template file thingy for dif types of headers if needed.

  print '<div id="apps-header" class="' . $template . ' ' . $_GET['headerType'] . '">';
  
  if (isset($_GET['headerContent'])){ 
    print $_GET['headerContent']; 
  } else {
    print 'Welcome to <b>' . $settings->frontend['site_name'] . '</b>';
  }
  
  print '</div>'; 

};

if ($headerType == 'appspage') { // ToDo: make template file thingy for dif types of headers if needed.

  print '<div id="apps-header" class="' . $template . ' ' . $headerType . '">';
  
  if (isset($_GET['headerContent'])){ 
    print $_GET['headerContent']; 
  } else {
    print 'Browsing <b>' . $groupInfo['name'] . '</b>';
  }
  
  print '</div>'; 

};

function generateImageLink(&$app){
  include('../settings.php');
  $appsDir = '/' . $settings->upload_dirs['root_dir'] . '/' . $settings->upload_dirs['icons'] . '/'  . $app['name_id'];
  $app['icon_url'] = $settings->frontend['def_app_icon'];
  $app['icon_size'] = [120, 120];
  if (file_exists(ROOT_DIR . $appsDir)){
    $files = getDirFiles(ROOT_DIR . $appsDir);
    if (count($files) > 0) {
      $app['icon_url'] = $files[0];
      $app['icon_size'] = getimagesize($app['icon_url']);
      if (count($files) > 1){
        $j = 1;
        while ($j < count($files)){  
          if (getimagesize($files[$j])[0] > $app['icon_size'][0]){
            $app['icon_url'] = $files[$j];
            $app['icon_size'] = getimagesize($files[$j]);
          }
          $j++;
        }
      }
      $app['icon_url'] = $appsDir . '/' . basename($app['icon_url']); // Replace file path with proper URL.
    }
  }
  return $app;
}

$i = count($groupApps);
$e = max($i - $range, 0);

while($i > $e){
  $i--; 
  if ($groupApps[$i]['is_group'] == 1){
    $childGroups[] = $groupApps[$i];
    unset($groupApps[$i]);
  }
}


$i = count($groupApps);
$e = max($i - $range, 0); 

if (count($groupApps) > 0){ // If groups has (visible) app(s).
  print '<div id="apps-container-heading" class="heading">Applications</div>';
  print '<div id="apps-container" class="' . $template . ' ' . $headerType .'">';

  // Print out apps.
  foreach($groupApps as $indx => $app){
    generateImageLink($app);
    include('../template/app.' . $template . '.template.php');
  }

  print '</div>';
}

if (count($childGroups) > 0){ // If groups has (visible) sub-group(s).
  print '<div id="child-groups" class="' . $template . ' ' . $headerType .'">';
  print '<div class="heading">Sub-categories</div>';
  print '<div id="child-groups-container">';
  $c = count($childGroups);
  $i = 0;
  while ($i < $c){
    $app = $childGroups[$i]; /* "$app" is required by template structure */
    generateImageLink($app);
    include('../template/app.' . $template . '.template.php');
    $i++;
  }
  print '</div></div>';
}
?>
