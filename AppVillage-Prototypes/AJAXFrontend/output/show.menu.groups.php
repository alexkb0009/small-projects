<?php

/** 
 *  Shows templated groups from sc-backend.
 *
 *  Parameters:
 *   - template: (optional) template file to use
 */

include('../data_source.sc_backend_xmlrpc.inc');

// Defaults
$template = 'menu-dropdown';

// Get Params if any.
if (isset($_GET['template'])) {
  $template = (string) $_GET['template'];
};

// XMLRPC Call for list of groups

$apps_groups = xmlrpc_call('sc.getGroupList', array());
$children_links = [];
$menu_object = [];

// 1st Depth
foreach ($apps_groups as $groupIndex => $groupInfo){
  $link = [];
  $link['int_id'] = $groupInfo['id'];
  $link['name'] = $groupInfo['name'];
  $link['name_id'] = $groupInfo['name_id'];

  if ($groupInfo['parent'] == 0){
    $menu_object[$groupInfo['id']] = $link;
  } else if ($groupInfo['parent'] > 0) {
    $link['parent'] = $groupInfo['parent'];
    $children_links[] = $link;
  }
}

// 2nd Depth
foreach ($children_links as $linkIndex => $link){
  if (isset($menu_object[$link['parent']])){
    $menu_object[$link['parent']]['children'][] = $link;
    unset($children_links[$linkIndex]);
  }
}


// Print & return items thru template (using $menu_object object).
include('../template/groups.' . $template . '.template.php');

?>
