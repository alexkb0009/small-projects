<?php

/** 
 *  Shows info for group to display in menubox next to group menu(s).
 *
 *  Parameters:
 *   - group_id: (required) id of the group of which to get info about.
 *   - detailed: (optional) shows extra detail about the app, such as permissions.
 *
 */

// Default Parameter Values
$group_id = NULL; // REQUIRE group_id 
$detailed = FALSE;
 
if (isset($_GET['group_id'])) {
  $group_id = (int) $_GET['group_id'];
};

if (isset($_GET['detailed'])) {
  $detailed = (bool) $_GET['detailed'];
};

include('../data_source.sc_backend_xmlrpc.inc');

$resultInfo = xmlrpc_call('sc.getGroupTextInfoByGroupID', array($group_id));

$description = strlen((string) $resultInfo['brief_info']) > strlen((string) $resultInfo['info']) ? $resultInfo['brief_info'] : $resultInfo['info'];

?>

<div class="heading">
  Information
</div>
<div id="group-name">
  <?php print $resultInfo['name']; ?>
</div>
<div id="group-description">
  <?php print strip_tags($description); ?>
</div>
