<?php

  /** Function to recursively display an array or object. Replacement for print_r. **/
  function debug_object($results, $description = NULL, $depth = 0){
    if ($depth == 0) {
      print('<pre style="border: 1px solid #e7e7e7; background: #e7e7e7; padding: 10px; display: inline-block; margin: 0;">');
      if ($description != NULL){
        print('<div style="border-bottom: 1px solid #ccc; padding-bottom: 3px; margin-bottom: 3px;">' . $description . '</div>');
      }
      if (is_array($results)){
        print("Array [");
      } else if (is_object($results)){
        print("Object {");
      } else {
        print("Not an array or object.");
        return False;
      }
    }
    foreach ((array) $results as $key => $result){
      if (is_array($result) || is_object($result)){
        print('<div style="padding-left: 30px; padding-bottom: 3px; padding-top: 5px; border-left: 1px solid #555;border-bottom: 1px solid #555; border-top: 1px solid #555; border-right: 5px solid rgba(0,90,180,0.25); background: rgba(0,0,0,0.1);margin-top: 3px;">');
        if (is_array($result)){
          print('<b>' . $key . '</b>: Array [');  
          debug_object($result, $description, $depth + 1);
          print(']</div>');
        } else if (is_object($result)){
          print('<b>' . $key . '</b>: Object {');  
          debug_object((array) $result, $description, $depth + 1);
          print('}</div>');
        }
      } else {
        print('<div style="padding: 3px; padding-left: 30px; padding-right: 20px; border-left: 1px solid #555; border-bottom: 1px solid #555;background: rgba(0,0,0,0.1);border-right: 5px solid rgba(0,0,200,0.25);"><b>' . $key . '</b>: ' . $result . '</div>');
      }
      //print('</div>');
    }
    if ($depth == 0){
      print((is_array($results) ? ']' : '}') . '</pre>');
    }
  }

  function getDirFiles($dir){
    $files = [];
    if ($handle = opendir($dir)){
      while (($file = readdir($handle)) !== false){
        if (!in_array($file, array('.', '..'))){
          $files[] = $dir . '/' . $file;
        }
      }
    }
    return $files;
  }
?>
