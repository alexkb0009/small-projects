<?php
  /** Unit Test for XML-RPC **/
  include_once("../data_source.sc_backend_xmlrpc.inc");


  xmlrpc_call('sc.getPackagesCount', array('name_id' => 347), $debug = TRUE);
  xmlrpc_call('sc.getNamesByGroupID', array('group_id' => 77), $debug = TRUE);
  xmlrpc_call('sc.getGroupList', array(), $debug = TRUE);

  xmlrpc_call('sc.getPackageListByNameID', array(353), $debug = TRUE);

  xmlrpc_call('sc.getPMPermissions', array('user_id' => 19), $debug = TRUE);
  xmlrpc_call('sc.getNameByNameID', array(353), $debug = TRUE);
  xmlrpc_call('sc.getProductNames', array(), $debug = TRUE);
  xmlrpc_call('sc.getProductList', array(), $debug = TRUE);
?>
