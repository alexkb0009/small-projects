<?php
  $children = [];
?>

<div id="groups-container">
<div class="heading">Categories</div>
<?php foreach ($menu_object as $groupIndex => $group): ?>

<div class="group item" id="group-menu-item-<?php print $group['int_id']; ?>" group_id="<?php print $group['int_id']; ?>">
  <?php 
    if (isset($group['children'])){
      $children[$group['int_id']] = $group['children'];
      print '<a href="#" class="expanded">' . $group['name'] . '</a>'; 
    } else {
      print '<a href="#">' . $group['name'] . '</a>'; 
    }
  ?>
  
</div>


<?php endforeach; ?>
</div>

<div id="groups-children-container" class="hidden">
<?php foreach ($children as $parentId => $childGroup): ?>
  <div class="menu-shell group-children" id="children-shell-<?php print $parentId; ?>">
    <div class="heading group-children"><?php print $menu_object[$parentId]['name']; ?></div>
    <?php foreach ($childGroup as $child): ?>
      <div class="group item child" id="group-menu-item-<?php print $child['int_id']; ?>" group_id="<?php print $child['int_id']; ?>">
        <a href="#"><?php print $child['name']; ?></a>
      </div>
    <?php endforeach; ?>
  </div>
<?php endforeach; ?>
</div>


<div id="groups-info-container">
  <div class="heading">Information</div>
</div>
