<?php
/**
 * Global HTML <head> Template/Content
 */
?>

<meta content="text/html; charset=utf-8" http-equiv="Content-Type"></meta>
<title><?php print $settings->frontend['site_name']; ?></title>


<!--- Stylesheets --->
<link media="all" href="template/style/layout.css" type="text/css" rel="stylesheet"></link>
<link media="all" href="template/style/global.css" type="text/css" rel="stylesheet"></link>
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Ar…0,700|Open+Sans:400italic,400,600,700,800,300|Nunito:400,700"></link>

<!--- JS --->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="template/js/third-party-includes/jquery.xmlrpc.min.js"></script>
<script type="text/javascript" src="template/js/data.ajaxMethods.js"></script>
<script type="text/javascript" src="template/js/ui.onload.js"></script>
