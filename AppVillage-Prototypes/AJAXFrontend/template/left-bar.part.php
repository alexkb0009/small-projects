<div id="leftBar-wrapper">
  <div id="leftBar">
    <div class="heading first">Explore Apps</div>
    <div class="inner first">

      <div class="section filters">
        <span class="label">Platform</span>
        <hr style="border: none;border-top: 1px solid #aaa;margin-right:0px;">
        <a class="expand-button-link filter" name="desktop">+</a> 
        Desktop
        <br>
        <div id="filter-box-desktop">
          <input name="desktop[windows]" type="checkbox">Windows<br>
          <input name="desktop[mac]" type="checkbox">Mac<br>
          <input name="desktop[linux]" type="checkbox">Linux<br>
          <input name="desktop[web]" type="checkbox">Web Apps<br>
         </div>
         <a class="expand-button-link filter" name="tablet">+</a>
         Tablet
         <br>
         <div id="filter-box-tablet">
           <input name="tablet[ipad]" type="checkbox">iPad<br>
           <input name="tablet[android]" type="checkbox">Android<br>
           <input name="tablet[other]" type="checkbox">Other<br>
         </div>
         <a class="expand-button-link filter" name="mobile">+</a>
         Mobile<br>
         <div id="filter-box-mobile">
           <input name="mobile[iphone]" type="checkbox">iPhone<br>
           <input name="mobile[android]" type="checkbox">Android Phone<br>
           <input name="mobile[winphone]" type="checkbox">Windows Phone<br>
           <input name="mobile[html5]" type="checkbox">HTML5<br>
           <input name="mobile[other]" type="checkbox">Other<br>
         </div>
       </div>

    
    </div>

    <div class="heading second">Recently Viewed</div>
    <div class="inner second">
      <div id="recently-viewed-links-container">

      </div>
    </div>

  </div>
</div>
