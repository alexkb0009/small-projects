<div id="headerArea">


  <div id="brandingArea">
    <a href="/" id="logo-container">
      <img src="<?php print $settings->frontend['header_icon_url']; ?>" id="logo">
    </a>
  </div>

  <div id="primary-menu-wrapper">
    <div id="primary-menu">

      <ul id="top-menu" class="menu">

        <!-- Menu Items -->

        <li class="leaf first">
          <a href="/" title="Home page" id="sc-menu-home" class="active">
            <?php print $settings->frontend['menu_labels']['home']; // Label from settings.php ?> 
          </a>
        </li>

        <li class="expanded">
          <a href="#" id="sc-menu-all-products">
            <?php print $settings->frontend['menu_labels']['all_products']; // Label from settings.php ?>
          </a>
          <div class="menu-shell all-products" id="groups-dropdown">
          </div>
        </li>
      
      </ul>

    </div>
  </div>


</div>
