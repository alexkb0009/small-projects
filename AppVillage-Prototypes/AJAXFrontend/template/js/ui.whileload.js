/**
 * Executed before page finishes loading. For setting layout and such before onready event is fired later. 
 */

/** These JS variables available globally, in other scripts **/

var contentArea = document.getElementById("contentArea"),
    contentAreaWrapper = document.getElementById("contentArea-wrapper"),
    headerArea = document.getElementById("headerArea"),
    leftBar = document.getElementById("leftBar"),
    menuGroups = document.getElementById("groups-dropdown"),
    appsMenuBox = null,
    s = {}, // Temp storage object.
    recentlyViewed = []; // Recently viewed apps. 

/* Layout Functions, updated onResize. */

function resizeFunc(){

  s.windowWidth = document.body.clientWidth || $('#body-area').innerWidth();
  $(menuGroups).removeClass('touch-bottom');
  if (s.windowWidth > 1170){
    contentAreaWrapper.style.width = (s.windowWidth - 320) + "px";
    $('body').removeClass('mobile');
    var menuGroupsHeight = ($("#groups-container .group.item").length * 28) + 54;
    var contentSpaceHeight = window.innerHeight - $(headerArea).outerHeight();
    if (menuGroupsHeight > contentSpaceHeight){
      menuGroups.style.height = contentSpaceHeight - 20 + 'px';
      $(menuGroups).addClass('touch-bottom');
    } else {
      menuGroups.style.height = menuGroupsHeight - 20 + 'px';
    }
    ui.topmenu.appsMenuBoxBaseWidth = s.windowWidth - 320 - 360 - 340 - 20;
  } else {
    contentAreaWrapper.style.width = "100%";
    $('body').addClass('mobile');
    menuGroups.style.height = "auto";
  }

  // Loader Icon
  ui.loader.loaderIcon.style.marginTop = ((window.innerHeight - $(headerArea).outerHeight() - 100) / 2) + 'px';
}  

function scrollFunc(){
  if (document.body.scrollTop > 22 && !$(document.body).hasClass('past_22px')){
    $(document.body).addClass('past_22px');
  } else if (document.body.scrollTop <= 22 && $(document.body).hasClass('past_22px')) {
    $(document.body).removeClass("past_22px");
  }

  if (document.body.scrollTop > 64 && !$(document.body).hasClass('past_64px')){
    $(document.body).addClass('past_64px');
  } else if (document.body.scrollTop <= 64 && $(document.body).hasClass('past_64px')) {
    $(document.body).removeClass("past_64px");
  }

}

var ui = {

  loader : {
    loaderIcon: document.createElement('div'),
    initLoaderIcon : function(){
      ui.loader.loaderIcon.id = "loader-icon";
      ui.loader.loaderIcon.innerHTML = '<img src="template/images/loader4-white.gif"><span>loading</span>';
    },
    displayLoaderIcon: function(){
      contentAreaWrapper.appendChild(ui.loader.loaderIcon);
    },
    destroyLoaderIcon: function(){
      contentAreaWrapper.removeChild(ui.loader.loaderIcon);
    }
  },

  filters : {
    toggleOpenBox: function(){
      if (this.innerHTML === '+'){
        this.innerHTML = "-";
        $('#filter-box-' + this.getAttribute('name')).slideDown();
      } else {
        this.innerHTML = "+";
        $('#filter-box-' + this.getAttribute('name')).slideUp();
      }
    }, 
    bindToggles: function(){
      $("div.filters.section a.expand-button-link").click(ui.filters.toggleOpenBox);
    }
  },

  recentlyViewed : {
    updateBlock: function(){
      var recentViewedLinksBlock = document.getElementById('recently-viewed-links-container');
      recentViewedLinksBlock.innerHTML = "";
      for (var i = recentlyViewed.length - 1; i >= 0; i--){
        var link = document.createElement('a');
        link.innerHTML = recentlyViewed[i].params.linkName;
        link.className = "recently-viewed-link";
        $(link).data({
          functionName: recentlyViewed[i].functionName,
          params: recentlyViewed[i].params
        });
        link.setAttribute("href","#");
        link.onclick = function(){
          window["ajaxMethods"][$(this).data('functionName')]($(this).data('params'));
          return false;
        }
        recentViewedLinksBlock.appendChild(link);
      }
    }
  },

  topmenu : {
    appsMenuBoxBaseWidth: 320,
    bindGroupsChildren: function(){

      $("div.menu-shell.all-products").on('mouseenter', '#groups-container .group.item', function(){
        $('#groups-container .group.item').removeClass('highlighted-parent');
        if ( $("#groups-children-container > #children-shell-" + this.getAttribute("group_id")).length == 0){
          $('#groups-children-container').addClass('hidden');
          if (s.windowWidth > 1170) $(appsMenuBox).width(ui.topmenu.appsMenuBoxBaseWidth + 340);
        } else {
          $("#groups-children-container > .group-children.visible").removeClass('visible');
          $("#groups-children-container > #children-shell-" + this.getAttribute("group_id")).addClass('visible');
          $('#groups-children-container').removeClass('hidden');
          $(this).addClass('highlighted-parent');
          if (s.windowWidth > 1170) $(appsMenuBox).width(ui.topmenu.appsMenuBoxBaseWidth);
        }

      });

      $("div.menu-shell.all-products").on('mouseenter', '.group.item', function(){
        $('#groups-container .group.item, #groups-children-container .group.item').removeClass('highlighted');
        ajaxMethods.menuAppsGroupsDescription({
          group_id: this.getAttribute('group_id')
        });
        $(this).addClass('highlighted');
        $(appsMenuBox).addClass("hidden");
      });

      $("div.menu-shell.all-products").on('click', '.group.item', function(){
        ajaxMethods.showAppsByGroupID({
          group_id: this.getAttribute('group_id'),
          linkName: $(this).children('a').html() + ' <small style="float: right;">(Category)</small>'
        });
        $("div.menu-shell.all-products").slideUp().parent().removeClass('open');
      });

    },

    bindClickKeepOpen: function(elementID){
      // If array, bind all elements in it.
      if (typeof elementID == 'array'){
        var i = 0;
        while (i < elementID.length){
          ui.topmenu.bindClickKeepOpen(elementID[i]);
          i++;
        }
      }

     // Bind single menu item (by dom id of link element (string)).
      $("#" + elementID).click(function(){
        var wasOpen = $(this).parent().hasClass("open");
        var parents = $(this).parent().parent().children(); 
        parents.removeClass('open');
        if (!wasOpen) {
          $(this).parent().addClass('open');
          $(this).next('.menu-shell').slideDown();
        } else {
          $(this).next('.menu-shell').slideUp();
        }
        return false;
      });
    }
  },

  buttons : {
    
  }

};

/** Variables in here are private context **/
(function($){

  /* 
      Begin Stuff 
  */

  /** Bind **/
  window.onresize = resizeFunc; /* May be overridden, e.g. in onReady functs, unless using += or jQ's $(window).resize(..) */
  window.onfocus = resizeFunc;
  window.onscroll = scrollFunc;

  /** Init **/
  ui.loader.initLoaderIcon();
  ui.filters.bindToggles();
  ui.topmenu.bindGroupsChildren();
  ui.topmenu.bindClickKeepOpen('sc-menu-all-products');
  resizeFunc();

  ajaxMethods.menuAppsGroups();

  ajaxMethods.showApps({ // Load front page.
    range: 200, 
    headerType: 'frontpage', 
    template: 'grid',
    linkName: 'View All Applications'
  });


})(jQuery);
