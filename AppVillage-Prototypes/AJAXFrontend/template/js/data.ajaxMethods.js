var ajaxMethods, callbacks;

(function($){

ajaxMethods = {
  currentRequest: {
    funcName: null,
    reqObj: null
  },
  showApps : function(paramsObj){
    ajaxMethods.currentRequest.reqObj = $.ajax({
      url: 'output/show.content.apps.all.php', 
      success: callbacks.toContentArea,
      data: paramsObj
    });
    ajaxMethods.currentRequest.funcName = 'showApps';
    uiUtil.saveToRecentlyViewed("showApps", paramsObj);
  },

  showAppsByGroupID : function(paramsObj){
    ajaxMethods.currentRequest.reqObj = $.ajax({
      url: 'output/show.content.apps.byGroupId.php', 
      success: callbacks.toContentArea,
      data: paramsObj
    });
    ajaxMethods.currentRequest.funcName = 'showAppsByGroupID';
    uiUtil.saveToRecentlyViewed("showAppsByGroupID", paramsObj);
  },

  menuAppsGroups : function(){
    ajaxMethods.currentRequest.reqObj = $.ajax({
      url: 'output/show.menu.groups.php', 
      complete: null,
      beforeSend: null,
      success: callbacks.menuAppsGroups,
    });
    ajaxMethods.currentRequest.funcName = 'menuAppsGroups';
  },

  menuAppsGroupsDescription : function(paramsObj){
    if (ajaxMethods.currentRequest.funcName == 'menuAppsGroupsDescription' && ajaxMethods.currentRequest.reqObj.readystate != 4){
      ajaxMethods.currentRequest.reqObj.abort();
    }
    ajaxMethods.currentRequest.reqObj = $.ajax({
      url: 'output/show.menubox.group.description.php',
      success: callbacks.toAppsMenuBox,
      complete: null,
      beforeSend: null,
      data: paramsObj,
      error: null
    });
    ajaxMethods.currentRequest.funcName = 'menuAppsGroupsDescription';
  }
}

callbacks = {
  // Function-specific
  toContentArea : function(obj, stat, resp){
    contentArea.innerHTML = obj;
    resizeFunc();
    $(appsMenuBox).width($("#groups-children-container").hasClass('hidden') == true ? (ui.topmenu.appsMenuBoxBaseWidth + 340) : (ui.topmenu.appsMenuBoxBaseWidth));
  },
  menuAppsGroups : function(obj, stat, resp){
    menuGroups.innerHTML = obj;
    resizeFunc();
  },
  toAppsMenuBox : function(obj, stat, resp){
    if (appsMenuBox == null) appsMenuBox = document.getElementById("groups-info-container");
    appsMenuBox.innerHTML = obj;
    $(appsMenuBox).removeClass('hidden');
  },
  // Defaults
  beforeAjaxSend : function(jqxhr, reqObj){
    ui.loader.displayLoaderIcon();
    $('body').addClass('loading');
    $('body').removeClass('load-complete');
  },
  error : function(obj, stat, resp){
    alert("Error! " + stat);
    console.log(resp);
  },
  complete : function(obj, stat, resp){
    resizeFunc();
    $('body').addClass('load-complete');
    ui.loader.destroyLoaderIcon();
    ui.recentlyViewed.updateBlock();
    $('body').removeClass('loading');
  }
}

uiUtil = {
  /** Save apps or views to recently viewed, to display @ left. **/
  saveToRecentlyViewed: function(functionName, paramsObj){
    if (typeof s !== 'undefined') { // s = global temp storage object.
      s.lastQuery = {};
      s.lastQuery.params = paramsObj;
      s.lastQuery.functionName = functionName;
      var duplicate = false;
      for (var i = 0; i < recentlyViewed.length; i++){
        if (s.lastQuery.functionName == recentlyViewed[i].functionName){
          for (var prop in s.lastQuery.params){ // Check each property; params == paramsObj isn't enough.
            if (s.lastQuery.params[prop] == recentlyViewed[i].params[prop]){
              duplicate = true;
            }
          }
        }
      }
      if (!duplicate) recentlyViewed.push(s.lastQuery);
      if (recentlyViewed.length > 5){
        recentlyViewed.shift();
      }
    }
  }
}

/*** AJAX DEFAULTS, may be overridden ***/

$.ajaxSetup({
  error: callbacks.error,
  complete: callbacks.complete,
  beforeSend: callbacks.beforeAjaxSend
});

})(jQuery)
