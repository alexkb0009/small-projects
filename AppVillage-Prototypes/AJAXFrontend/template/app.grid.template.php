<div class="app grid" name_id="<?php print $app['name_id']; ?>">
  <?php if ($app['icon_url'] != NULL): ?>
    <div class="app-grid icon-container">
      <img src="<?php print $app['icon_url']; ?>" class="app-grid icon" style="padding-top: <?php print ((120 - min($app['icon_size'][1], 120)) / 2); ?>px;">
    </div>
  <?php endif; ?>
  <div class="app-grid name">
    <?php print (strlen($app['name']) > 25) ? substr($app['name'], 0, 22) . '...' : $app['name']; ?>
  </div>
</div>

