<?php
    
    $settings = new stdClass(); // Generic Object used for state.

    /** SC-BACKEND running process || XML-RPC Default **/
    /** Important: if not working, make sure this is correct: **/
    define('AV_SERV', 'http://appvillage1.ics.com:9000');

    $settings->frontend = [
      "site_name"       => "AppVillage by ICS",
      "header_bg_url"   => "http://alexb.ddns.me/testing/ics/appvillage/mockup-php-ajax-testing/appvilBanner7.jpg",
      "header_icon_url" => "template/images/av-logo-old.png",
      "menu_labels"     => [
        "home"            => "Home",
        "all_products"    => "Apps"
      ],
      "def_app_icon"    => "template/images/app-default-icon-2.png"
    ];

    /** Set directories for user-uploaded files, don't include trailing/preceding slashes. **/
    $settings->upload_dirs = [
      "root_dir"   => "uploaded_files",
      "images"     => "apps/images",
      "thumbnails" => "apps/thumbnails",
      "icons"      => "apps/icons"
    ];


    define('ROOT_DIR', dirname(__FILE__)); // Define Root Dir.

?>
