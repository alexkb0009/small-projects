<?php

  include_once('settings.php'); // Include settings, available to all functions here once loaded.

  /** Connect to AppVillage Backend Program via XMLRPC & return results as PHP array. ***/ 
  function xmlrpc_call($requestMethod, $parameters, $debug = FALSE, $host = AV_SERV){
    
    $header[] = "Content-type: text/xml";
    $header[] = "Content-length: " . strlen($requestMethod);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $host);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, xmlrpc_encode_request($requestMethod, $parameters));
    $result = curl_exec($ch);
    if (curl_errno($ch)){
      print(curl_error($ch));
    } else {
      curl_close($ch);
      $result = xmlrpc_decode($result);
      if ($debug == FALSE) return $result; // Return output if not debugging.

      /** (else) Print/Debug Output Before Returning **/
      include_once('utilities.inc');
      print('<br>');
      debug_object($parameters, "Parameters");
      print('<br>');
      debug_object($result, $description = 'Result from <b>\'' . $requestMethod . '\'</b>');
      return $result;
    }
  }

?>
