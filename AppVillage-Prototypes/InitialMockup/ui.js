


function init(){

  // Loads in homepage (unneeded for drupal) & taking over backbutton + history
  getData('home');
  if (browser.IEv > 9){
    window.onpopstate = function(event) {
      if (!event.state){
//        getData('home'); // If no history related directly to this site visit, load in homepage.
      } else {
        getData(event.state, true); // fetches from history, which is set in getdata funct (true means don't add fetched data to history)
      }
    };
  } else {getData('home'); alert("You are running Internet Explorer 9.0 or below, your back button will not work within this site. We suggest Chrome, Firefox, or Safari.")}



  // --------- //


  // Category Button & dropdown
  $("#catButton").hover(function(){
    $("#catDropDown").css("visibility","visible");
  },function(){
    $("#catDropDown").css("visibility","hidden");
  });
  
  //Sets initial dWrapper width when page loads.
  reSize();


  // Top right searchfield value defs.
  var searchField = {
    field: $("#edit-search-theme-form-1"),
    val1:  $("#edit-search-theme-form-1").val(),
    val2: "",
    color1: $("#edit-search-theme-form-1").css("color")
  }


  // Top right searchfield functs.

  searchField.field.focus(function(){
    if (searchField.field.val() == searchField.val1){
      searchField.field.val(searchField.val2);//.css("color","#fff");
    };
  });
  searchField.field.blur(function(){
    if (searchField.field.val() == searchField.val2){
      searchField.field.val(searchField.val1);//.css("color", searchField.color1);
    };
  });



  var advOpts = {
    open: false,
    indicator: $('#advSearchOptsInd'),
    box: $('#advSearchOpts'),
    link: $('#advSearchOptLink')
  }

  // Open + Close Filtering Options + Clear on close.
  advOpts.link.click(function(){
    if (advOpts.open == false){
      advOpts.open = true;
      advOpts.indicator.html('Hide Options&nbsp;&nbsp;&nbsp;<b><</b>');
//      advOpts.link.css('background-image', 'url(moreOptsArrow2.png)'); // For use if/when background graphic used instd of ">"
      advOpts.box.css('width', '535px').css('visibility', 'visible');
      
    }
    else {
      advOpts.open = false;
      advOpts.indicator.html('Show Filtering Options&nbsp;&nbsp;&nbsp;<b>></b>');
//      advOpts.link.css('background-image', 'url(moreOptsArrow.png)');
      advOpts.box.css('width', '0px').css('visibility', 'hidden');
      resetSearch();
    }
  }).mousedown(function(e){e.preventDefault()});

}

// Functions to be used throughout UI below.

// Automatic resizing of elements to fit browser width. Called each time browser width changes, as well as hiding of rightColumn, and on page load.
function reSize(){
  $("#dWrapper").width($("#pageWrapper").width() - $("#leftBar").outerWidth() - 10 /* Left Column Margin */);

    if ($("#rightColumn").css('display') == 'none'){
      
      $("#hpContent").css('margin-right','-10px').css('padding-right','16px');
      $("#contentArea").width( $("#dWrapper").width() - 20 );
    } else {
      $("#hpContent").css('margin-right','-212px').css('padding-right','225px');
      $("#contentArea").width( $("#dWrapper").width() - 40 - $("#rightColumn").outerWidth());
    }
    $("#leftWhiteBar").css('height', $("#hpContent").height() + 32 /* hpCont padding */);
}

// Toggles sub-options for platform show/hide, 'all' label, and unselects suboptions on uncheck. 
function chkToggle(chkBox, divBox){
  if($(chkBox).html() == '+'){
    $(divBox).css('display','block');
    $(chkBox).html("-");
  } else {
    $(divBox).css('display','none');
    $(chkBox).html("+");
  }
}


// Unchecks all suboptions of platform.
function uncheckAll(divBox){
  $(divBox + " input:checkbox").removeAttr('checked');
}

// Unchecks all options (reset/clear) when closing filtering options and pressing clear button.
function resetSearch(){
  $('#advSearchOpts input:checkbox').removeAttr('checked');
  $("#desktopAllLabel, #tabletAllLabel, #mobileAllLabel").css('display','none');
  
}

// ------ Carousel Functions.

carousel = {
  slideCount: 0,
  currentIndex: 1
}


function slideChg(dir){
  carousel.slideCount = $('.slideTitleTextBox').length;
  $("#sImage_" + carousel.currentIndex + ", #sTsT_" + carousel.currentIndex).css('opacity', '0.25');
  $("#sTsT_" + carousel.currentIndex).css('margin', '25px -90px 0 150px');
  $("#sThumb_" + carousel.currentIndex).css('color', '#58A3C9').css('border-color','#58A3C9');
  if (dir == 'next'){
    if (carousel.currentIndex < carousel.slideCount){
      carousel.currentIndex++;
    } else {
      carousel.currentIndex = 1; 
    }
  } else if (dir == 'prev') {
    if (carousel.currentIndex > 1){
      carousel.currentIndex--;
    } else {
      carousel.currentIndex = carousel.slideCount;
    } 
  } else if (isNaN(dir) == false) {
    carousel.currentIndex = dir;
  }

  $("#sThumb_" + carousel.currentIndex).css('color','#ffffff').css('border-color','#ffffff');
  $("#sImage_" + carousel.currentIndex + ", #sTsT_" + carousel.currentIndex).css('opacity', '1');
  $("#sTsT_" + carousel.currentIndex).css('margin', '25px 0 0 60px');
  $("#slideCarousel").css('margin-left', -(carousel.currentIndex - 1) * 800 + "px");
}

// -----------

window.onresize = reSize; // Binds window resizing to resize function.

window.onload = init; // Runs onLoad functions & bindings.

window.unload = function(){goBack()};