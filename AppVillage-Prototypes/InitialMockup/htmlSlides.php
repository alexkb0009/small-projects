<?php  

##  $con = mysql_connect('mysql4.000webhost.com','a5594702_appvil','appvil1');
##  mysql_select_db("a5594702_appvil", $con);

##  $resultA = ""; 

  #### Setup this query to relationally fetch needed data (TBD), most likely author name by username_author of app, download link, description, image.

  #### Below content will go into multicontainer div, currently defined in parent php file (getData.php), or be a part of this file.

  #### Suggested way of generating content is running SQL query and '$i = 1, while($row = mysql_fetch_array($resultA)){ i++ ...' generate this starting with sImage_1, sTsT_1, etc. up to sImage_4, etc. w/ echo "<img class="slideImage" id='sImage_'" . i . "' src='" . urlOfImage . "'> ...";
  #### ^ Not necessary.

  #### Add .showImg and .showSTST class to first slide.
?>

<div id="slideFull"> <!--- Auto Margin Centering Container for variable-width layout --->
<div id="slideCarousel">

  <img id="sImage_1" class="slideImage showImg" src="geolocation.jpg">

  <div id="sTsT_1" class="slideTitleTextBox showSTST">

    <div id="sTitle_1" class="slideTitle">
      GeoAlarm in HTML5
    </div>

    <div id="sText_1" class="slideText">
      <span>
        A test application implementing Google Maps API to create an alarm application, which is activated when within x distance of target destination. Required: GPS signal, internet connection when setting destination, HTML5 Video capable browser. If finished/todos: Save destinations (not require internet for commonly used ones), changing HTML5 video to audio, with several formats of audio/video as backup.
      </span>
    </div>

    <a id="sLink_1" class="slideLink" href="#" onclick="getData('app:htmlgeoalarm'); return false"> 
      Check out GeoAlarm >>
    </a>

  </div>

<!--- Next Slide --->

  <img id="sImage_2" class="slideImage" src="" style="visibility: hidden;">
  <!--- 
    If no image for app, just leave src="" for once there is a default background image.
    Or add 'style="visibility: hidden;"'
   --->

  <div id="sTsT_2" class="slideTitleTextBox">

    <div id="sTitle_2" class="slideTitle">
      Yet Another App Example
    </div>

    <div id="sText_2" class="slideText">
      <span>
        Yet Another App Example is an example of the data that might go into what makes an instance of an app on this site. Proposed data welcome. Questions to decides include how many screenshots, one or many, whether to use separate table(s) for these screenshots. Next up is to create a table of users and authors and relationally map to the apps table, so that authors can manage their apps as well as better data organization, e.g. in their Author homepages (to only be defined once per author, viewed on all their apps).
      </span>
    </div>

    <a id="sLink_2" class="slideLink" href="#" onclick="getData('app:someapp3'); return false"> 
      Check out Yet Another App Example >>
    </a>

  </div>

<!--- Next Slide --->

  <img id="sImage_3" class="slideImage" src="http://ics.com/img/front/tiles/nface.png">

  <div id="sTsT_3" class="slideTitleTextBox">

    <div id="sTitle_3" class="slideTitle">
      The North Face Snow Report
    </div>

    <div id="sText_3" class="slideText">
      <span>
        The North Face Snow Report is an application where you can... etc. etc.
      </span>
    </div>

    <a id="sLink_3" class="slideLink" href="#" onclick="getData('app:snowreport'); return false"> 
      Check out The North Face Snow Report >>
    </a>

  </div>

<!--- Next Slide --->

  <img id="sImage_4" class="slideImage" src="http://ics.com/img/front/tiles/nface.png">

  <div id="sTsT_4" class="slideTitleTextBox">

    <div id="sTitle_4" class="slideTitle">
      The North Face Snow Report (test slide #4)
    </div>

    <div id="sText_4" class="slideText">
      <span>
        The North Face Snow Report is an application where you can... etc. etc.
      </span>
    </div>

    <a id="sLink_4" class="slideLink" href="#" onclick="getData('app:snowreport'); return false"> 
      Check out The North Face Snow Report >>
    </a>

  </div>

<!--- End Slides --->
</div>
</div>

<div id="slideControls">
<a id="slideGoPrev" onclick="slideChg('prev')">&nbsp;</a>
<a id="slideGoNext" onclick="slideChg('next')">&nbsp;</a>
</div>

<div id="slideThumbs">
<a onclick="slideChg(1)" id="sThumb_1" style="color: #fff;">1</a>
<a onclick="slideChg(2)" id="sThumb_2">2</a>
<a onclick="slideChg(3)" id="sThumb_3">3</a>
<a onclick="slideChg(4)" id="sThumb_4">4</a>
</div>