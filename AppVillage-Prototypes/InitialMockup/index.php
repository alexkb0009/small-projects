<?php
$con = mysql_connect('localhost','testing_access','testpassword');
mysql_select_db("appvil_mockup", $con);
if (!$con){
  die("Connection Error");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="style.css" media="all" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Arvo:400,700,400italic|Ubuntu:400,500,700|Podkova:400,700|Open+Sans:400italic,400,600,700,800,300|Nunito:400,700' rel='stylesheet' type='text/css'>
    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="ui.js" type="text/javascript"></script>
    <script src="jquery.history.js"></script>
    <script type="text/javascript">

  // Check for IE (only 10+ is supported for HTML5 back button)
    var browser = { 
      IEp: navigator.userAgent.search(/msie/i),
      IEv: 10
    };
    if(browser.IEp!=-1){
      browser.IEv = navigator.userAgent.substring(browser.IEp + 5,browser.IEp + 8);
    }; 

	var currentStr = "home";
	var recentlyViewed = [];

	function getData(str, getHist){
	  currentStr = str;
	  if (str == ""){
	    document.getElementById("contentArea").innerHTML="";
	    return;
	  } 
	  if (window.XMLHttpRequest){
	    xmlhttp=new XMLHttpRequest();
	  }
	  else { // code for IE6, IE5
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function(){
	    if (xmlhttp.readyState==4 && xmlhttp.status==200){
	      document.getElementById("contentArea").innerHTML=xmlhttp.responseText;
	      if (str == "home"){
	        reSize();
		$("#leftWhiteBar").css('display','block');
	      } else {
		$("#leftWhiteBar").css('display','none');
	      }
	    }
	  }
	  xmlhttp.open("GET","getData.php?q="+str,true);
	  xmlhttp.send();
	  if (str.substring(0,4) == "cat:"){
	    document.getElementById("searchTitle").innerHTML="Search in Category";
	  } else if (str == "all"){
	    document.getElementById("searchTitle").innerHTML="Search in Results";
	  } else { 
	    document.getElementById("searchTitle").innerHTML="Search for Apps"; 
	  }
	  if (str.substring(0,4) == "app:"){
	    recentlyViewed.push(str);
	  }
	  if (!getHist && browser.IEv > 9){	    
	    history.pushState(currentStr, "Page: " + currentStr, "?page=" + currentStr); // Unneeded for drupal
	  }
	}

    </script>
</head>

<body>
    <div id="headerArea">

      <div id="headerBG">&nbsp;</div>

      <a href="/"><img src="logo5.png" style="margin: 3px 0 3px 42px;border:none;z-index:10;"></a>

      <div id="loginRegister">
	Login or Register
      </div>

      <div id="topMenu">

	<ul id="topMenuList">
	  <li id="homeLink"><a href="" onclick="getData('home') ;return false;">Home</a></li>
	  <li id="appsLink"><div class="button" id="catButton">
	    Apps
	    <div id="catDropDown">
		<?php
		  $sql = "SELECT * FROM categories";
		  $result = mysql_query($sql);
		    while($row = mysql_fetch_array($result)){
		      echo "<div class='categoryItem' onClick=\"getData('cat:" . $row['cat_id'] . "')\">" . $row['cat_name'] . "</div>";
		    };
		?>
	    </div>
	  </a>
	  </li>
	  <li id="communityLink"><a href="/appvillage/">My Apps</a></li>
 	  <li id="communityLink"><a href="/appvillage/">Support</a></li>
	  <li id="communityLink"><a href="/appvillage/">Manage</a></li>
	  <li id="helpLink"><a href="/appvillage/">Help</a></li>
	</ul>

	<div id="genSearch">
	  <form action="/" onsubmit="return false;" accept-charset="UTF-8" method="post" id="search-theme-form">
	    <input value="Search site content" type="text" maxlength="128" name="search_theme_form" id="edit-search-theme-form-1" size="15" title="Enter the terms you wish to search for">
	    <input style="width: 50px;" type="submit" name="op" id="edit-submit-1" value="Go">
	  </form>
	</div>

      </div>

    </div>

    <div id="pageWrapper">

      <div id="leftWhiteBar">&nbsp;</div>

      <div id="leftBar">
        <div class="leftHeader" id="searchTitle">
	  Search for Apps
        </div>

        <div class="leftInner">

	  <form action="" onsubmit="getData('all'); return false">

	    <input type="text" name="appSearch" class="leftForm" style="width: 148px;margin-left:-12px;"> 
	    <input type="submit" value="Go" class="leftForm" style="position: absolute; left: 182px;margin-right: 0;width: 50px;">

            <!--- The Button to open Advanced Opts, check ui.js for action. --->

	    <div id="advSearchOptLink">
	      &nbsp;<span id="advSearchOptsInd" style="float:right;margin-right:0px;">Show Filtering Options&nbsp;&nbsp;&nbsp;<b>></b></span>
	    </div>

	    <!--- The Advanced Search Box --->
	    <div id="advSearchOpts" style="width: 0px; visibility: hidden;">
	      <div style="min-width: 400px;">
	      <div class="advSection">
	        Platform<hr style="border: none;border-top: 1px solid #aaa;margin-right:0px;">
	        <a onclick="chkToggle(this, '#moreDesktop'); return false">+</a> Desktop<br>
	        <div id="moreDesktop" style="margin: 2px 0 10px 15px; display:none;">
	          <input type="checkbox" value="">Windows<br>
	          <input type="checkbox" value="">Mac<br>
	          <input type="checkbox" value="">Linux<br>
	          <input type="checkbox" value="">Web Apps<br>
	        </div>
	        <a onclick="chkToggle(this, '#moreTablet'); return false">+</a> Tablet<br>
	        <div id="moreTablet" style="margin: 2px 0 10px 15px;display:none;">
	          <input type="checkbox" value="">iPad<br>
	          <input type="checkbox" value="">Android<br>
	          <input type="checkbox" value="">Other<br>
	        </div>
	        <a onclick="chkToggle(this, '#moreMobile'); return false">+</a> Mobile<br>
	        <div id="moreMobile" style="margin: 2px 0 10px 15px;display:none;">
	          <input type="checkbox" value="">iPhone<br>
	          <input type="checkbox" value="">Android Phone<br>
	          <input type="checkbox" value="">Windows Phone<br>
	          <input type="checkbox" value="">Symbian<br>
	          <input type="checkbox" value="">HTML5<br>
	          <input type="checkbox" value="">Other<br>
	        </div>
	      </div>

	      <div class="advSection">
	        Category <hr style="border: none;border-top: 1px solid #aaa;margin-right:0px;">
	        <input type="checkbox" value="">Google Apps<br>
	        <input type="checkbox" value="">Open Source<br>
	        <input type="checkbox" value="">Productivity<br>
	        <input type="checkbox" value="">Experimental<br>
	        <input type="checkbox" value="">Games<br>
	      </div>

	      <div class="advSection">
	        Price <hr style="border: none;border-top: 1px solid #aaa;margin-right:0px;">
	        <input type="checkbox" value="">Free<br>
	        <input type="checkbox" value="">Free to try<br>
	        <input type="checkbox" value="">Purchase<br>
	      </div>
	      
	   
	      <div style="position: absolute;right:20px;bottom:20px;">
	        <input type="submit" value="Go" class="leftForm" style="margin-left: 2px;width: 50px;border:none" onclick="$('#advSearchOptLink').click();">
	        <!---<input type="reset" value="Clear" class="leftForm" style="width: 50px;border:none" onclick="resetSearch()"> For clearing all options, probably not needed --->
	      </div>
	      
	      </div><!---Min-width wrapper--->
	    </div>
	  </form>
        </div>

        <div class="leftHeader">
	  Recently Viewed
        </div>
        <div class="leftInner">

	  <div class="button" onClick="getData('all')">All</div>
	  <div class="button" onClick="getData('home')">Featured [Home]</div>
	  <div class="button" onClick="getData('app:someApp')">Grab "Some App"</div>
	  <div class="button" onClick="getData('app:htmlgeoalarm')">Grab "HTML5 GeoAlarm"</div>
        </div>
	<br>
        <div class="leftHeader">
	  Upload an App
        </div>
        <div class="leftInner">
	  Please log in or register
        </div>

      </div>
      <div id="dWrapper">
        <div id="contentArea">&nbsp;</div>


	<div id="rightColumn">
	  <a style="display: block; padding: 1px 4px; color: #fff;" onclick="$('#rightColumn').css('display','none');$('#pageWrapper').css('min-width', '1004px'); reSize()">Hide</a>
	  &nbsp;
	</div>

      </div>

      <div id="belowContent">

	<div id="footerCopyright">
	  &copy; 2012 Integrated Computer Solutions, Inc., All rights reserved.
	  <span id="copyrightLinks"><a href="#">Contact Us</a> | <a href="#">Privacy Policy</a> | <a href="#">Trademarks</a></span>
	</div>

	<div id="footerBlockHeader">
	  <div id="footerBlockWrapper" style="width: 1200px; margin: 0 auto;">
	    <div class="footerBlock">
	      <b>My Apps</b><hr>
	      <a onclick="getData('app:htmlgeoalarm')">GeoAlarm in HTML5</a><br>
	    </div>

	    <div class="footerBlock">
	      <b>Apps</b><hr>
	      <a onclick="getData('cat:1')">Google Apps</a><br> <!--- Generate this similar to code in dropdown above --->
	      <a onclick="getData('cat:2')">Open Source</a><br>
	      <a onclick="getData('cat:3')">Productivity</a><br>
	      <a onclick="getData('cat:4')">Experimental</a><br>
	      <a onclick="getData('cat:5')">Games</a><br>
	    </div>

	    <div class="footerBlock">
	      <b>Support</b><hr>
	      <a onclick="getData('support:downloads')">Downloads</a> <br>
	      <a onclick="getData('support:faqs')">FAQs</a><br>
	      <a onclick="getData('support:forums')">Forums</a><br>
	      <a onclick="getData('support:wikis')">Wikis</a><br>
	      <a onclick="getData('support:videos')">Videos</a><br>
	      <a onclick="getData('support:report_issue')">Report Issue</a><br>
	      <a onclick="getData('support:ask_expert')">Ask the Expert</a><br>
	      <a onclick="getData('support:track_issue')">Issue Tracking</a><br> 
	      <a onclick="getData('support:about')">About AppVillage</a><br> <!--- Static htmlpage, echoed or included from php --->
	    </div>

	    <div class="footerBlock">
	      <b>Manage</b><hr>
	    </div>

	    <div class="footerBlock">
	      <b>Administrate</b><hr>
	    </div>

	  </div>
	</div>

      </div>

    </div>

</body>

</html>
<?php
mysql_close($con);
?>