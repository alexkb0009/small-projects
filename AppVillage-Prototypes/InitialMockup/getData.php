<?php
$q = $_GET["q"]; ## Sent by calling JS function.
$con = mysql_connect('localhost','testing_access','testpassword');
mysql_select_db("appvil_mockup", $con);

if (!$con){
  die("Connection Error");
}

if ($q == 1){
  echo "Hello, 1 was called";
}

if ($q == 'home'){ ## Show homepage/featured.
  echo "<div id='pageTitle'>Featured App (Currently placeholder for interactive content/slideshow)</div>";

#### This code fetches and populates featured apps. Will not be used in current V. Look at htmlslides.php for suggested implementation of generating individual slides.
  
##  $sql1 = "SELECT apps.full_name,featured.img,featured.unique_name AS unique_name 
##	  FROM apps,featured WHERE apps.unique_name = featured.unique_name 
##	  ORDER BY featured.id ASC";

##
##  $resultA = mysql_query($sql1);
  echo "<div id='multiContainer'>";
##  while($row = mysql_fetch_array($resultA)){
##    echo "<div class='hpAppCont' 
##	       style='background: url(" . $row['img'] . ") 0px 0px;'
##	       onclick=\"getData('app:" . $row['unique_name'] . "')\">";
##    echo "<a class='hpAppName'>" . $row['full_name'] . "</a><br>";
##    ##if ($row['img']) echo "<br><br>" . $row['img'] . "<br>";

##    echo "</div>";
##  }

  include "htmlSlides.php";

  echo "</div>";


##### Code Below fetches and populates Google Apps from featured SQL table. Will not be used as Google Apps removed from design.
##### SQL Query below is same as for featured apps.

##  echo "<table id='hpTable'><tr><td>";
##  echo "<div id='hpGoogleApps'>";
##  echo "<div class='hpHeader'>Google Apps</div><br>";
##
##  $resultB = mysql_query($sql1);

##  while($row = mysql_fetch_array($resultB)){
##    echo "<div class='hpGoogleApp' 
##	       style='background: url(" . $row['img'] . ") 0px 0px;'
##	       onclick=\"getData('app:" . $row['unique_name'] . "')\">";
##    echo "<a class='gaAppName'><span style='background: #fff;'>" . $row['full_name'] . "</span></a><br>";
##    ##if ($row['img']) echo "<br><br>" . $row['img'] . "<br>";
##    echo "</div>";
##  }
##  echo "</div>";
##  echo "</td><td rowspan=2 valign=top>";


  echo "<div id='hpContent'>";

  echo "<div id='hpCommunity'>";
  echo "<span class='hpHeader'>Recent News & Updates</span><br><br>";

  echo "<div class='hpPost'><span style='color: #111; font-size: 1.3em;font-family: Arial;'>";
  echo "A Post Title Goes Here";
  echo "</span><br>";
  echo "<span style='font-size: .9em;'>Posted Friday, 05/04/2012, by <b>Elizabeth Warren</b></span><br><br>";
  echo "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  echo "</div>";

  echo "<div class='hpPost'><span style='color: #111; font-size: 1.3em;font-family: Arial;'>";
  echo "A Post Title Goes Here";
  echo "</span><br>";
  echo "<span style='font-size: .9em;'>Posted Friday, 05/04/2012, by <b>Elizabeth Warren</b></span><br><br>";
  echo "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  echo "</div>";

  echo "</div>";

##  echo "</td></tr><tr><td valign=top>";
##  echo "<div id='hpNewestApps'>";
##  echo "<div class='hpHeader'>Newest Apps</div>";
##  echo "";
##  echo "</div>";
##  echo "</td></tr></table>";

  echo "</div>";
  ##echo "<script>function(){ $('#homeLink').css('background-image','url(icons/home-icon-active.png)')};</script>";
}

if ($q == 'all'){ ## Show all apps.
  echo "<div id='pageTitle'>All Applications</div>";
  $sql = "SELECT * FROM apps";
  $result = mysql_query($sql);
  echo "<div id='multiContainer2'>";
  while($row = mysql_fetch_array($result)){
    echo "<div class='brAppCont'
	       style='background:url(" . $row['imgurl'] . ")'
               onclick=\"getData('app:" . $row['unique_name'] . "')\">";
    echo "<a class='brAppName'>" . $row['full_name'] . "</a><br>";
    echo "</div>";
  }
  echo "</div>";
}

if (substr($q, 0, 4) == "cat:"){ ## Show all apps within category.
  $cat = substr($q, 4);
  $sql = "SELECT * FROM categories,apps WHERE (categories.cat_id = " . $cat . " AND FIND_IN_SET('" . $cat . "', apps.categories)>0 )";
  $result = mysql_query($sql);
  if (mysql_num_rows($result) == 0) {
    echo "No Apps in category " . $cat . ".";
  } else {
    echo "<br>" .  mysql_result($result, 0, 'cat_name');
    mysql_data_seek($result, 0);
    echo "<div id='multiContainer2'>";
    while($row = mysql_fetch_array($result)){
     ## echo  $row['cat_name'];
      echo "<div class='brAppCont'
	         style='background:url(" . $row['imgurl'] . ")'
                 onclick=\"getData('app:" . $row['unique_name'] . "')\">";
      echo "<a class='brAppName'>" . $row['full_name'] . "</a><br>";
      echo "</div>";
    }
    echo "</div>";
  }
}


if (substr($q, 0, 4) == "app:"){ ## Show an individual app + details
  $app = substr($q, 4);

  ##$sql = "SELECT * FROM apps WHERE unique_name='" . $app . "'";
  $sql = "SELECT apps.full_name,apps.desc,apps.price,apps.download_url,users.full_name AS author,users.author_url AS author_url 
	  FROM apps,users WHERE (apps.unique_name = '" . $app . "' AND apps.author = users.user_name)";

  $result = mysql_query($sql);
  while($row = mysql_fetch_array($result)){
    echo "<div id='pageTitle'>" . $row['full_name'] . "</div>";
    echo "<div id='appContainer'>";
      ##echo "<div id='appName'>" . $row['full_name'] . "</div><br>";
      echo "<span id='appDesc'>" . $row['desc']. "</span><br>";
      echo "<br><span id='appPrice'>Cost: ";
        if($row['price'] == 0){echo "Free";} else {echo "$" . $row['price'];}; 
      echo "</span><br><br>";

      echo "<span id='appAuthor'>By: ";
	if ($row['author_url']) {echo "<a id='authorLink' target='_blank' href='". $row['author_url'] . "'>" . $row['author'] . "</a>";}
	else {echo $row['author'];}
      echo "</span><br>";
      echo "<a id='appDownload' target='_blank' href='". $row['download_url'] . "'>Go to App Download Page</a></span>" . "<br>";
      echo "<br>Reviews & Comments: <br>";
    echo "</div>";
  }

  
} 

mysql_close($con);
##echo "<div style='clear:both; margin: 20px 0 0 0;'>end PHP (generated server-side, app data from MySQL db)</div>";

?>